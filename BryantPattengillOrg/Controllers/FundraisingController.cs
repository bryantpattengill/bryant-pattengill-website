﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BryantPattengillOrg.Models;
using Microsoft.AspNetCore.Hosting;
using BryantPattengillOrg.Services;
using BryantPattengillOrg.Models.Board;
using BryantPattengillOrg.Models.Events;

namespace BryantPattengillOrg.Controllers
{
    public class FundraisingController : Controller
    {
        private readonly IHostingEnvironment _Env;
        private readonly IPageDataService _PageData;
        
        public FundraisingController(
           IHostingEnvironment env, IPageDataService pageData)
        {
            _Env = env;
            _PageData = pageData;
        }


        [Route("/fundraising")]
        public IActionResult Index()
        {

            return View();
        }

        [Route("/fundraising/shopper-loyalty-programs")]
        public IActionResult ShopperLoyaltyPrograms()
        {
            return View();
            
        }

        [Route("/fundraising/shop-at-amazonsmile")]
        public IActionResult AmazonSmile()
        {
            
            return View();
        }

        [Route("/fundraising/box-tops-for-education")]
        public IActionResult BoxTopsForEducation()
        {

            return View();
        }

        [Route("/donate")]
        public IActionResult Donate()
        {
            return View();
        }

        [Route("/donate/readathon")]
        [Route("/donate/rat")]
        [Route("/donate/read-a-thon")]
        public IActionResult ReadAThonRedirect()
        {
            return RedirectPermanent("/events/read-a-thon#donate");
        }

        

        [Route("/donate/read-a-thon/thank-you")]
        public IActionResult DonateReadAThonThankYou()
        {
            return View();
        }

        [Route("/annual-dues")]
        public IActionResult AnnualDues()
        {

            return View();
        }

        [Route("/fundraising/grocery-scrip")]
        public IActionResult GroceryScrip()
        {
            return RedirectPermanent("/fundraising/shopper-loyalty-programs");

        }

        [Route("/fundraising/clothing-drives")]
        public IActionResult ClothingDrives()
        {

            return View();
        }



        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
