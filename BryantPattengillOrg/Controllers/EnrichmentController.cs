﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BryantPattengillOrg.Models;
using BryantPattengillOrg.Services;

namespace BryantPattengillOrg.Controllers
{
    public class EnrichmentController : Controller
    {

        [Route("/enrichment")]
        public IActionResult Index()
        {
           
            return View();
        }

        [Route("/what-we-fund")]
        public IActionResult WhatWeFund()
        {

            return View();
        }

    }
}
