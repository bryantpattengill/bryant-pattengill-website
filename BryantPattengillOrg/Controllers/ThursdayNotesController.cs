﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BryantPattengillOrg.Models;
using BryantPattengillOrg.Services;
using Microsoft.AspNetCore.Hosting;

namespace BryantPattengillOrg.Controllers
{
    public class ThursdayNotesController : Controller
    {
        private readonly IHostingEnvironment _Env;
        private readonly IPageDataService _PageData;

        public ThursdayNotesController(
           IHostingEnvironment env, IPageDataService pageData)
        {
            _Env = env;
            _PageData = pageData;
        }

        
        
        [Route("/thursday-notes")]
        public IActionResult Index()
        {

            return View();
        }

        private string GetTitle(DateTime date)
        {
            return $"Thursday Notes: {date.ToLongDateString()}";
        }

        private string GetDescription(DateTime date)
        {
            return $"Thursday Notes: {date.ToLongDateString()}";
        }


        // attempt to use notes in pages file
        //[Route("/thursday-notes/{year}-{month}-{day}")]
        //public IActionResult Detail(int year, int month, int day)
        //{
        //    var page = (from p in _PageData.OldPosts
        //                //where p.Type == OldPostType.ThursdayNotes
        //                //&& 
        //                where p.ConvertedDateTime.Year == year
        //                && p.ConvertedDateTime.Month == month
        //                && p.ConvertedDateTime.Day == day

        //                select p).FirstOrDefault();

        //    if (page == null)
        //    {
        //        return NotFound();
        //    }

        //    var date = new DateTime(year, month, day);
        //    ViewData["Cananocal"] = $"https://www.bryantpattengill.org/thursday-notes/{year}/{month}/{day}";
        //    ViewData["Title"] = GetTitle(date);
        //    ViewData["Description"] = GetDescription(date);
        //    //ViewData["Image"] = null;
        //    //return View();

        //    ViewData["DisplayDate"] = page.ConvertedDateTime.ToString("MMMM dd, yyyy");
        //    //ViewData["Created"] = page.PostDate;
        //    //ViewData["CreatedGMT"] = page.PostDateGMT;
        //    //ViewData["Title"] = page.Title;
        //    //ViewData["OriginalId"] = page.PostName;
        //    //ViewData["OriginalLink"] = page.Link;
        //    //ViewData["Category"] = page.Category;
        //    //ViewData["PostType"] = page.PostType;
        //    //ViewData["Status"] = page.Status;

        //    return View(page);
        //}
        #region OldThursdayNotes
        [Route("/thursday-notes/2014-08-25")]
        public IActionResult Notes20140825()
        {
            var date = new DateTime(2014, 8, 25);
            ViewData["Cananocal"] = "https://www.bryantpattengill.org/2014/08/25/bryantpattengill-thursday-notes-4/";
            ViewData["Title"] = GetTitle(date);
            ViewData["Description"] = GetDescription(date);
            ViewData["Image"] = null;
            return View();
        }
        [Route("/thursday-notes/2014-09-04")] public IActionResult Notes20140904() { var date = new DateTime(2014, 9, 4); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2014/09/04/bryantpattengill-thursday-notes-5/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2014-09-11")] public IActionResult Notes20140911() { var date = new DateTime(2014, 9, 11); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2014/09/11/bryantpattengill-thursday-notes/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2014-09-18")] public IActionResult Notes20140918() { var date = new DateTime(2014, 9, 18); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2014/09/18/bryantpattengill-thursday-notes-2/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2014-09-25")] public IActionResult Notes20140925() { var date = new DateTime(2014, 9, 25); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2014/09/25/bryantpattengill-thursday-notes-3/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2014-10-02")] public IActionResult Notes20141002() { var date = new DateTime(2014, 10, 2); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2014/10/02/bryantpattengill-thursday-notes-6/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2014-10-09")] public IActionResult Notes20141009() { var date = new DateTime(2014, 10, 9); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2014/10/09/bryantpattengill-thursday-notes-october-9-2014/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = "/wp-content/uploads/2014/10/image.jpg"; return View(); }
        [Route("/thursday-notes/2014-10-16")] public IActionResult Notes20141016() { var date = new DateTime(2014, 10, 16); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2014/10/16/250/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = "/wp-content/uploads/2014/10/applePyramid.jpg"; return View(); }
        [Route("/thursday-notes/2014-10-23")] public IActionResult Notes20141023() { var date = new DateTime(2014, 10, 23); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2014/10/23/thursday-notes/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = "/wp-content/uploads/2014/10/gardennews.jpg"; return View(); }
        [Route("/thursday-notes/2014-10-30")] public IActionResult Notes20141030() { var date = new DateTime(2014, 10, 30); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2014/10/30/bryantpattengill-thursday-notes-october-30-2014/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = "/wp-content/uploads/2014/10/PumpkinArt.jpg"; return View(); }
        [Route("/thursday-notes/2014-11-06")] public IActionResult Notes20141106() { var date = new DateTime(2014, 11, 6); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2014/11/06/thursday-notes-november-6th-2014/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = "/wp-content/uploads/2014/11/IMG_7068.jpg"; return View(); }
        [Route("/thursday-notes/2014-11-13")] public IActionResult Notes20141113() { var date = new DateTime(2014, 11, 13); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2014/11/13/thursday-notes-2/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = "/wp-content/uploads/2014/11/hands.png"; return View(); }
        [Route("/thursday-notes/2014-11-20")] public IActionResult Notes20141120() { var date = new DateTime(2014, 11, 20); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2014/11/20/thursday-notes-3/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = "/wp-content/uploads/2014/11/Proud-Dads-Club.png"; return View(); }
        [Route("/thursday-notes/2014-12-04")] public IActionResult Notes20141204() { var date = new DateTime(2014, 12, 4); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2014/12/04/thursday-notes-december-4th-2014/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = "/wp-content/uploads/2014/10/bryant-_1.jpg"; return View(); }
        [Route("/thursday-notes/2014-12-11")] public IActionResult Notes20141211() { var date = new DateTime(2014, 12, 11); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2014/12/11/thursday-notes-for-december-11th-2014/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = "/wp-content/uploads/2014/12/cropped-IMG_4501-e1417763304813.jpg"; return View(); }
        [Route("/thursday-notes/2014-12-18")] public IActionResult Notes20141218() { var date = new DateTime(2014, 12, 18); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2014/12/18/thursday-notes-4/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = "/wp-content/uploads/2014/12/studentnutcrackers.jpg"; return View(); }
        [Route("/thursday-notes/2015-01-08")] public IActionResult Notes20150108() { var date = new DateTime(2015, 1, 8); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2015/01/08/thursday-notes-5/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = "/wp-content/uploads/2015/01/bryant-iciles.jpg"; return View(); }
        [Route("/thursday-notes/2015-01-15")] public IActionResult Notes20150115() { var date = new DateTime(2015, 1, 15); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2015/01/15/thursday-notes-6/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = "/wp-content/uploads/2015/01/bryant-books.jpg"; return View(); }
        [Route("/thursday-notes/2015-01-22")] public IActionResult Notes20150122() { var date = new DateTime(2015, 1, 22); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2015/01/23/thursday-notes-january-22-2015/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = "/wp-content/uploads/2015/01/bryant-frames.jpg"; return View(); }
        [Route("/thursday-notes/2015-01-29")] public IActionResult Notes20150129() { var date = new DateTime(2015, 1, 29); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2015/01/29/thursday-notes-january-29-2015/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = "/wp-content/uploads/2014/11/Support-Art-Education-Header.jpg"; return View(); }
        [Route("/thursday-notes/2015-02-05")] public IActionResult Notes20150205() { var date = new DateTime(2015, 2, 5); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2015/02/05/thursday-notes-7/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = "/wp-content/uploads/2015/02/penguin-art.jpg"; return View(); }
        [Route("/thursday-notes/2015-02-12")] public IActionResult Notes20150212() { var date = new DateTime(2015, 2, 12); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2015/02/12/thursday-notes-february-12-2015/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = "/wp-content/uploads/2015/02/penguin-art.jpg"; return View(); }
        [Route("/thursday-notes/2015-02-26")] public IActionResult Notes20150226() { var date = new DateTime(2015, 2, 26); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2015/02/26/thursday-notes-february-26-2015/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = "/wp-content/uploads/2015/02/child-in-snow.jpg"; return View(); }
        [Route("/thursday-notes/2015-03-05")] public IActionResult Notes20150305() { var date = new DateTime(2015, 3, 5); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2015/03/05/thursday-notes-march-5-2015/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = "/wp-content/uploads/2015/03/pj-night.jpg"; return View(); }
        [Route("/thursday-notes/2015-03-12")] public IActionResult Notes20150312() { var date = new DateTime(2015, 3, 12); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2015/03/12/thursday-notes-march-12-2015/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = "/wp-content/uploads/2014/12/studentnutcrackers.jpg"; return View(); }
        [Route("/thursday-notes/2015-03-19")] public IActionResult Notes20150319() { var date = new DateTime(2015, 3, 19); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2015/03/19/1262/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = "/wp-content/uploads/2015/03/storytimeatbryant.jpg"; return View(); }
        [Route("/thursday-notes/2015-03-26")] public IActionResult Notes20150326() { var date = new DateTime(2015, 3, 26); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2015/03/26/thursday-notes-march-26-2015/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = "/wp-content/uploads/2015/01/bryant-frames.jpg"; return View(); }
        [Route("/thursday-notes/2015-04-02")] public IActionResult Notes20150402() { var date = new DateTime(2015, 4, 2); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2015/04/02/thursday-notes-april-2-2015/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = "/wp-content/uploads/2014/10/image.jpg"; return View(); }
        [Route("/thursday-notes/2015-04-16")] public IActionResult Notes20150416() { var date = new DateTime(2015, 4, 16); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2015/04/16/thursday-notes-8/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = "/wp-content/uploads/2015/04/ThursdayNotesApril16.jpg"; return View(); }
        [Route("/thursday-notes/2015-04-23")] public IActionResult Notes20150423() { var date = new DateTime(2015, 4, 23); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2015/04/23/thursday-notes-april-23-2015/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = "/wp-content/uploads/2015/04/flags.jpg"; return View(); }
        [Route("/thursday-notes/2015-04-30")] public IActionResult Notes20150430() { var date = new DateTime(2015, 4, 30); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2015/04/30/thursday-notes-april-30-2015/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = "/wp-content/uploads/2015/04/day-of-service.jpg"; return View(); }
        [Route("/thursday-notes/2015-05-07")] public IActionResult Notes20150507() { var date = new DateTime(2015, 5, 7); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2015/05/07/thursday-notes-may-7-2015/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2015-05-14")] public IActionResult Notes20150514() { var date = new DateTime(2015, 5, 14); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2015/05/14/thursday-notes-may-14-2015/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = "/wp-content/uploads/2015/04/IMG_7498.jpg"; return View(); }
        [Route("/thursday-notes/2015-05-21")] public IActionResult Notes20150521() { var date = new DateTime(2015, 5, 21); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2015/05/21/thursday-notes-may-21-2015/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = "/wp-content/uploads/2014/10/bryant-elementary-ann-arbor.jpg"; return View(); }
        [Route("/thursday-notes/2015-05-28")] public IActionResult Notes20150528() { var date = new DateTime(2015, 5, 28); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2015/05/28/thursday-notes-may-28-2015/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2015-06-04")] public IActionResult Notes20150604() { var date = new DateTime(2015, 6, 4); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2015/06/04/thursday-notes-june-4-2015/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = "/wp-content/uploads/2015/06/IMG_8211.jpg"; return View(); }
        [Route("/thursday-notes/2015-06-11")] public IActionResult Notes20150611() { var date = new DateTime(2015, 6, 11); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2015/06/11/thursday-notes-june-11-2015/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = "/wp-content/uploads/2015/04/flags.jpg"; return View(); }
        [Route("/thursday-notes/2015-09-10")] public IActionResult Notes20150910() { var date = new DateTime(2015, 9, 10); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2015/09/10/thursday-notes-9/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = "/wp-content/uploads/2015/09/artflower.jpg"; return View(); }
        [Route("/thursday-notes/2015-09-17")] public IActionResult Notes20150917() { var date = new DateTime(2015, 9, 17); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2015/09/18/thursday-notes-09-17-15/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2015-09-24")] public IActionResult Notes20150924() { var date = new DateTime(2015, 9, 24); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2015/09/25/thursday-notes-09-24-15/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = "/wp-content/uploads/2015/09/pltw-thurs-notes.png"; return View(); }
        [Route("/thursday-notes/2015-10-01")] public IActionResult Notes20151001() { var date = new DateTime(2015, 10, 1); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2015/10/02/thursday-notes-10-01-15/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2015-10-08")] public IActionResult Notes20151008() { var date = new DateTime(2015, 10, 8); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2015/10/08/thursday-notes-10-08-15/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2015-10-15")] public IActionResult Notes20151015() { var date = new DateTime(2015, 10, 15); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2015/10/15/thursday-notes-10-15-15/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2015-10-22")] public IActionResult Notes20151022() { var date = new DateTime(2015, 10, 22); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2015/10/22/thursday-notes-10-22-15/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2015-11-05")] public IActionResult Notes20151105() { var date = new DateTime(2015, 11, 5); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2015/11/06/thursday-notes-11-05-15/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2015-11-12")] public IActionResult Notes20151112() { var date = new DateTime(2015, 11, 12); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2015/11/12/thursday-notes-11-12-15/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2015-11-19")] public IActionResult Notes20151119() { var date = new DateTime(2015, 11, 19); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2015/11/19/thursday-notes-11-19-15/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2015-12-03")] public IActionResult Notes20151203() { var date = new DateTime(2015, 12, 3); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2015/12/04/thursday-notes-12-03-15/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2015-12-10")] public IActionResult Notes20151210() { var date = new DateTime(2015, 12, 10); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2015/12/11/thursday-notes-12-10-15/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2015-12-17")] public IActionResult Notes20151217() { var date = new DateTime(2015, 12, 17); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2015/12/17/thursday-notes-12-17-15/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2016-01-07")] public IActionResult Notes20160107() { var date = new DateTime(2016, 1, 7); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2016/01/07/thursday-notes-1-07-16/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2016-01-14")] public IActionResult Notes20160114() { var date = new DateTime(2016, 1, 14); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2016/01/14/thursday-notes-01-14-16/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2016-01-21")] public IActionResult Notes20160121() { var date = new DateTime(2016, 1, 21); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2016/01/22/thursday-notes-1-21-16/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2016-01-28")] public IActionResult Notes20160128() { var date = new DateTime(2016, 1, 28); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2016/01/29/thursday-notes-1-28-16/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2016-02-04")] public IActionResult Notes20160204() { var date = new DateTime(2016, 2, 4); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2016/02/04/thursday-notes-02-04-16/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2016-02-11")] public IActionResult Notes20160211() { var date = new DateTime(2016, 2, 11); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2016/02/10/thursday-notes-02-11-16/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2016-02-25")] public IActionResult Notes20160225() { var date = new DateTime(2016, 2, 25); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2016/02/25/thursday-notes-02-25-16/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2016-03-03")] public IActionResult Notes20160303() { var date = new DateTime(2016, 3, 3); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2016/03/03/thursday-notes-3-03-16/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2016-03-10")] public IActionResult Notes20160310() { var date = new DateTime(2016, 3, 10); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2016/03/09/thursday-notes-3-10-16/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2016-03-17")] public IActionResult Notes20160317() { var date = new DateTime(2016, 3, 17); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2016/03/16/thursday-notes-031716/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2016-03-24")] public IActionResult Notes20160324() { var date = new DateTime(2016, 3, 24); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2016/03/24/2007/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2016-03-31")] public IActionResult Notes20160331() { var date = new DateTime(2016, 3, 31); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2016/03/31/thursday-notes-3-31-16/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2016-04-14")] public IActionResult Notes20160414() { var date = new DateTime(2016, 4, 14); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2016/04/14/thursday-notes-4-14-16/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2016-04-21")] public IActionResult Notes20160421() { var date = new DateTime(2016, 4, 21); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2016/04/22/thursday-notes-4-21-16/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2016-04-28")] public IActionResult Notes20160428() { var date = new DateTime(2016, 4, 28); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2016/04/28/thursday-notes-4-28-16/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2016-05-05")] public IActionResult Notes20160505() { var date = new DateTime(2016, 5, 5); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2016/05/05/thursday-notes-5-5-16/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2016-05-12")] public IActionResult Notes20160512() { var date = new DateTime(2016, 5, 12); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2016/05/13/thursday-notes-5-12-16/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2016-05-19")] public IActionResult Notes20160519() { var date = new DateTime(2016, 5, 19); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2016/05/19/thursday-notes-5-19-16/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2016-06-02")] public IActionResult Notes20160602() { var date = new DateTime(2016, 6, 2); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2016/06/03/thursday-notes-6-02-16/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2016-06-09")] public IActionResult Notes20160609() { var date = new DateTime(2016, 6, 9); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2016/06/10/thursday-notes-6-09-16/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2016-06-16")] public IActionResult Notes20160616() { var date = new DateTime(2016, 6, 16); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2016/06/16/thursday-notes-6-16-16/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2016-09-08")] public IActionResult Notes20160908() { var date = new DateTime(2016, 9, 8); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2016/09/09/thursday-notes-09-08-16/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2016-09-15")] public IActionResult Notes20160915() { var date = new DateTime(2016, 9, 15); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2016/09/16/thursday-notes-09-15-15/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2016-09-22")] public IActionResult Notes20160922() { var date = new DateTime(2016, 9, 22); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2016/09/22/thursday-notes-09-22-16/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2016-09-29")] public IActionResult Notes20160929() { var date = new DateTime(2016, 9, 29); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2016/09/30/thursday-notes-09-29-16/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2016-10-06")] public IActionResult Notes20161006() { var date = new DateTime(2016, 10, 6); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2016/10/07/thursday-note-10-06-16/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2016-10-13")] public IActionResult Notes20161013() { var date = new DateTime(2016, 10, 13); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2016/10/14/thursday-notes-10-13-16/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2016-10-20")] public IActionResult Notes20161020() { var date = new DateTime(2016, 10, 20); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2016/10/20/thursday-notes-10-20-16/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2016-10-27")] public IActionResult Notes20161027() { var date = new DateTime(2016, 10, 27); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2016/10/28/thursday-notes-10-27-16/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2016-11-10")] public IActionResult Notes20161110() { var date = new DateTime(2016, 11, 10); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2016/11/11/thursday-notes-1110-16/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2016-11-17")] public IActionResult Notes20161117() { var date = new DateTime(2016, 11, 17); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2016/11/18/thursday-notes-11-17-16/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2016-12-01")] public IActionResult Notes20161201() { var date = new DateTime(2016, 12, 1); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2016/12/01/thursday-notes-12-1-16/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2016-12-08")] public IActionResult Notes20161208() { var date = new DateTime(2016, 12, 8); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2016/12/09/2344/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2016-12-22")] public IActionResult Notes20161222() { var date = new DateTime(2016, 12, 22); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2016/12/21/thursday-notes-12-22-16/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2017-01-12")] public IActionResult Notes20170112() { var date = new DateTime(2017, 1, 12); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2017/01/12/thursday-notes-1-12-17/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2017-01-19")] public IActionResult Notes20170119() { var date = new DateTime(2017, 1, 19); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2017/01/19/thursday-notes-1-19-17/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2017-01-26")] public IActionResult Notes20170126() { var date = new DateTime(2017, 1, 26); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2017/01/27/thursday-notes-1-26-17/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2017-02-02")] public IActionResult Notes20170202() { var date = new DateTime(2017, 2, 2); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2017/02/02/thursday-notes-2-2-17/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2017-02-09")] public IActionResult Notes20170209() { var date = new DateTime(2017, 2, 9); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2017/02/10/thursday-notes-2-9-17/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2017-02-16")] public IActionResult Notes20170216() { var date = new DateTime(2017, 2, 16); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2017/02/17/thursday-notes-2-16-17/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2017-02-23")] public IActionResult Notes20170223() { var date = new DateTime(2017, 2, 23); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2017/02/23/thursday-notes-2-23-17/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2017-03-02")] public IActionResult Notes20170302() { var date = new DateTime(2017, 3, 2); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2017/03/05/thursday-notes-3-2-17/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2017-03-09")] public IActionResult Notes20170309() { var date = new DateTime(2017, 3, 9); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2017/03/10/thursday-notes-3-9-17/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2017-03-23")] public IActionResult Notes20170323() { var date = new DateTime(2017, 3, 23); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2017/03/24/thursday-notes-3-23-17/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2017-03-30")] public IActionResult Notes20170330() { var date = new DateTime(2017, 3, 30); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2017/03/30/thursday-notes-3-30-17/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2017-04-13")] public IActionResult Notes20170413() { var date = new DateTime(2017, 4, 13); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2017/04/13/thursday-notes-4-13-17/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2017-04-20")] public IActionResult Notes20170420() { var date = new DateTime(2017, 4, 20); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2017/04/20/thursday-notes-4-20-17/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2017-04-27")] public IActionResult Notes20170427() { var date = new DateTime(2017, 4, 27); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2017/04/27/thursday-notes-4-27-17/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2017-05-04")] public IActionResult Notes20170504() { var date = new DateTime(2017, 5, 4); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2017/05/05/thursday-notes-5-4-17/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2017-05-11")] public IActionResult Notes20170511() { var date = new DateTime(2017, 5, 11); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2017/05/11/thursday-notes-5-11-17/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }
        [Route("/thursday-notes/2017-06-01")] public IActionResult Notes20170601() { var date = new DateTime(2017, 6, 1); ViewData["Cananocal"] = "https://www.bryantpattengill.org/2017/06/01/thursday-notes-6-1-17/"; ViewData["Title"] = GetTitle(date); ViewData["Description"] = GetDescription(date); ViewData["Image"] = null; return View(); }


        #endregion

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }


        #region ArchivedOldNotesRedirect
        [Route("/2014/08/25/bryantpattengill-thursday-notes-4/")] public IActionResult Notes20140825Redirect() { return RedirectPermanent("/thursday-notes/2014-08-25"); }
        [Route("/2014/09/04/bryantpattengill-thursday-notes-5/")] public IActionResult Notes20140904Redirect() { return RedirectPermanent("/thursday-notes/2014-09-04"); }
        [Route("/2014/09/11/bryantpattengill-thursday-notes/")] public IActionResult Notes20140911Redirect() { return RedirectPermanent("/thursday-notes/2014-09-11"); }
        [Route("/2014/09/18/bryantpattengill-thursday-notes-2/")] public IActionResult Notes20140918Redirect() { return RedirectPermanent("/thursday-notes/2014-09-18"); }
        [Route("/2014/09/25/bryantpattengill-thursday-notes-3/")] public IActionResult Notes20140925Redirect() { return RedirectPermanent("/thursday-notes/2014-09-25"); }
        [Route("/2014/10/02/bryantpattengill-thursday-notes-6/")] public IActionResult Notes20141002Redirect() { return RedirectPermanent("/thursday-notes/2014-10-02"); }
        [Route("/2014/10/09/bryantpattengill-thursday-notes-october-9-2014/")] public IActionResult Notes20141009Redirect() { return RedirectPermanent("/thursday-notes/2014-10-09"); }
        [Route("/2014/10/16/250/")] public IActionResult Notes20141016Redirect() { return RedirectPermanent("/thursday-notes/2014-10-16"); }
        [Route("/2014/10/23/thursday-notes/")] public IActionResult Notes20141023Redirect() { return RedirectPermanent("/thursday-notes/2014-10-23"); }
        [Route("/2014/10/30/bryantpattengill-thursday-notes-october-30-2014/")] public IActionResult Notes20141030Redirect() { return RedirectPermanent("/thursday-notes/2014-10-30"); }
        [Route("/2014/11/06/thursday-notes-november-6th-2014/")] public IActionResult Notes20141106Redirect() { return RedirectPermanent("/thursday-notes/2014-11-06"); }
        [Route("/2014/11/13/thursday-notes-2/")] public IActionResult Notes20141113Redirect() { return RedirectPermanent("/thursday-notes/2014-11-13"); }
        [Route("/2014/11/20/thursday-notes-3/")] public IActionResult Notes20141120Redirect() { return RedirectPermanent("/thursday-notes/2014-11-20"); }
        [Route("/2014/12/04/thursday-notes-december-4th-2014/")] public IActionResult Notes20141204Redirect() { return RedirectPermanent("/thursday-notes/2014-12-04"); }
        [Route("/2014/12/11/thursday-notes-for-december-11th-2014/")] public IActionResult Notes20141211Redirect() { return RedirectPermanent("/thursday-notes/2014-12-11"); }
        [Route("/2014/12/18/thursday-notes-4/")] public IActionResult Notes20141218Redirect() { return RedirectPermanent("/thursday-notes/2014-12-18"); }
        [Route("/2015/01/08/thursday-notes-5/")] public IActionResult Notes20150108Redirect() { return RedirectPermanent("/thursday-notes/2015-01-08"); }
        [Route("/2015/01/15/thursday-notes-6/")] public IActionResult Notes20150115Redirect() { return RedirectPermanent("/thursday-notes/2015-01-15"); }
        [Route("/2015/01/23/thursday-notes-january-22-2015/")] public IActionResult Notes20150122Redirect() { return RedirectPermanent("/thursday-notes/2015-01-22"); }
        [Route("/2015/01/29/thursday-notes-january-29-2015/")] public IActionResult Notes20150129Redirect() { return RedirectPermanent("/thursday-notes/2015-01-29"); }
        [Route("/2015/02/05/thursday-notes-7/")] public IActionResult Notes20150205Redirect() { return RedirectPermanent("/thursday-notes/2015-02-05"); }
        [Route("/2015/02/12/thursday-notes-february-12-2015/")] public IActionResult Notes20150212Redirect() { return RedirectPermanent("/thursday-notes/2015-02-12"); }
        [Route("/2015/02/26/thursday-notes-february-26-2015/")] public IActionResult Notes20150226Redirect() { return RedirectPermanent("/thursday-notes/2015-02-26"); }
        [Route("/2015/03/05/thursday-notes-march-5-2015/")] public IActionResult Notes20150305Redirect() { return RedirectPermanent("/thursday-notes/2015-03-05"); }
        [Route("/2015/03/12/thursday-notes-march-12-2015/")] public IActionResult Notes20150312Redirect() { return RedirectPermanent("/thursday-notes/2015-03-12"); }
        [Route("/2015/03/19/1262/")] public IActionResult Notes20150319Redirect() { return RedirectPermanent("/thursday-notes/2015-03-19"); }
        [Route("/2015/03/26/thursday-notes-march-26-2015/")] public IActionResult Notes20150326Redirect() { return RedirectPermanent("/thursday-notes/2015-03-26"); }
        [Route("/2015/04/02/thursday-notes-april-2-2015/")] public IActionResult Notes20150402Redirect() { return RedirectPermanent("/thursday-notes/2015-04-02"); }
        [Route("/2015/04/16/thursday-notes-8/")] public IActionResult Notes20150416Redirect() { return RedirectPermanent("/thursday-notes/2015-04-16"); }
        [Route("/2015/04/23/thursday-notes-april-23-2015/")] public IActionResult Notes20150423Redirect() { return RedirectPermanent("/thursday-notes/2015-04-23"); }
        [Route("/2015/04/30/thursday-notes-april-30-2015/")] public IActionResult Notes20150430Redirect() { return RedirectPermanent("/thursday-notes/2015-04-30"); }
        [Route("/2015/05/07/thursday-notes-may-7-2015/")] public IActionResult Notes20150507Redirect() { return RedirectPermanent("/thursday-notes/2015-05-07"); }
        [Route("/2015/05/14/thursday-notes-may-14-2015/")] public IActionResult Notes20150514Redirect() { return RedirectPermanent("/thursday-notes/2015-05-14"); }
        [Route("/2015/05/21/thursday-notes-may-21-2015/")] public IActionResult Notes20150521Redirect() { return RedirectPermanent("/thursday-notes/2015-05-21"); }
        [Route("/2015/05/28/thursday-notes-may-28-2015/")] public IActionResult Notes20150528Redirect() { return RedirectPermanent("/thursday-notes/2015-05-28"); }
        [Route("/2015/06/04/thursday-notes-june-4-2015/")] public IActionResult Notes20150604Redirect() { return RedirectPermanent("/thursday-notes/2015-06-04"); }
        [Route("/2015/06/11/thursday-notes-june-11-2015/")] public IActionResult Notes20150611Redirect() { return RedirectPermanent("/thursday-notes/2015-06-11"); }
        [Route("/2015/09/10/thursday-notes-9/")] public IActionResult Notes20150910Redirect() { return RedirectPermanent("/thursday-notes/2015-09-10"); }
        [Route("/2015/09/18/thursday-notes-09-17-15/")] public IActionResult Notes20150917Redirect() { return RedirectPermanent("/thursday-notes/2015-09-17"); }
        [Route("/2015/09/25/thursday-notes-09-24-15/")] public IActionResult Notes20150924Redirect() { return RedirectPermanent("/thursday-notes/2015-09-24"); }
        [Route("/2015/10/02/thursday-notes-10-01-15/")] public IActionResult Notes20151001Redirect() { return RedirectPermanent("/thursday-notes/2015-10-01"); }
        [Route("/2015/10/08/thursday-notes-10-08-15/")] public IActionResult Notes20151008Redirect() { return RedirectPermanent("/thursday-notes/2015-10-08"); }
        [Route("/2015/10/15/thursday-notes-10-15-15/")] public IActionResult Notes20151015Redirect() { return RedirectPermanent("/thursday-notes/2015-10-15"); }
        [Route("/2015/10/22/thursday-notes-10-22-15/")] public IActionResult Notes20151022Redirect() { return RedirectPermanent("/thursday-notes/2015-10-22"); }
        [Route("/2015/11/06/thursday-notes-11-05-15/")] public IActionResult Notes20151105Redirect() { return RedirectPermanent("/thursday-notes/2015-11-05"); }
        [Route("/2015/11/12/thursday-notes-11-12-15/")] public IActionResult Notes20151112Redirect() { return RedirectPermanent("/thursday-notes/2015-11-12"); }
        [Route("/2015/11/19/thursday-notes-11-19-15/")] public IActionResult Notes20151119Redirect() { return RedirectPermanent("/thursday-notes/2015-11-19"); }
        [Route("/2015/12/04/thursday-notes-12-03-15/")] public IActionResult Notes20151203Redirect() { return RedirectPermanent("/thursday-notes/2015-12-03"); }
        [Route("/2015/12/11/thursday-notes-12-10-15/")] public IActionResult Notes20151210Redirect() { return RedirectPermanent("/thursday-notes/2015-12-10"); }
        [Route("/2015/12/17/thursday-notes-12-17-15/")] public IActionResult Notes20151217Redirect() { return RedirectPermanent("/thursday-notes/2015-12-17"); }
        [Route("/2016/01/07/thursday-notes-1-07-16/")] public IActionResult Notes20160107Redirect() { return RedirectPermanent("/thursday-notes/2016-01-07"); }
        [Route("/2016/01/14/thursday-notes-01-14-16/")] public IActionResult Notes20160114Redirect() { return RedirectPermanent("/thursday-notes/2016-01-14"); }
        [Route("/2016/01/22/thursday-notes-1-21-16/")] public IActionResult Notes20160121Redirect() { return RedirectPermanent("/thursday-notes/2016-01-21"); }
        [Route("/2016/01/29/thursday-notes-1-28-16/")] public IActionResult Notes20160128Redirect() { return RedirectPermanent("/thursday-notes/2016-01-28"); }
        [Route("/2016/02/04/thursday-notes-02-04-16/")] public IActionResult Notes20160204Redirect() { return RedirectPermanent("/thursday-notes/2016-02-04"); }
        [Route("/2016/02/10/thursday-notes-02-11-16/")] public IActionResult Notes20160211Redirect() { return RedirectPermanent("/thursday-notes/2016-02-11"); }
        [Route("/2016/02/25/thursday-notes-02-25-16/")] public IActionResult Notes20160225Redirect() { return RedirectPermanent("/thursday-notes/2016-02-25"); }
        [Route("/2016/03/03/thursday-notes-3-03-16/")] public IActionResult Notes20160303Redirect() { return RedirectPermanent("/thursday-notes/2016-03-03"); }
        [Route("/2016/03/09/thursday-notes-3-10-16/")] public IActionResult Notes20160310Redirect() { return RedirectPermanent("/thursday-notes/2016-03-10"); }
        [Route("/2016/03/16/thursday-notes-031716/")] public IActionResult Notes20160317Redirect() { return RedirectPermanent("/thursday-notes/2016-03-17"); }
        [Route("/2016/03/24/2007/")] public IActionResult Notes20160324Redirect() { return RedirectPermanent("/thursday-notes/2016-03-24"); }
        [Route("/2016/03/31/thursday-notes-3-31-16/")] public IActionResult Notes20160331Redirect() { return RedirectPermanent("/thursday-notes/2016-03-31"); }
        [Route("/2016/04/14/thursday-notes-4-14-16/")] public IActionResult Notes20160414Redirect() { return RedirectPermanent("/thursday-notes/2016-04-14"); }
        [Route("/2016/04/22/thursday-notes-4-21-16/")] public IActionResult Notes20160421Redirect() { return RedirectPermanent("/thursday-notes/2016-04-21"); }
        [Route("/2016/04/28/thursday-notes-4-28-16/")] public IActionResult Notes20160428Redirect() { return RedirectPermanent("/thursday-notes/2016-04-28"); }
        [Route("/2016/05/05/thursday-notes-5-5-16/")] public IActionResult Notes20160505Redirect() { return RedirectPermanent("/thursday-notes/2016-05-05"); }
        [Route("/2016/05/13/thursday-notes-5-12-16/")] public IActionResult Notes20160512Redirect() { return RedirectPermanent("/thursday-notes/2016-05-12"); }
        [Route("/2016/05/19/thursday-notes-5-19-16/")] public IActionResult Notes20160519Redirect() { return RedirectPermanent("/thursday-notes/2016-05-19"); }
        [Route("/2016/06/03/thursday-notes-6-02-16/")] public IActionResult Notes20160602Redirect() { return RedirectPermanent("/thursday-notes/2016-06-02"); }
        [Route("/2016/06/10/thursday-notes-6-09-16/")] public IActionResult Notes20160609Redirect() { return RedirectPermanent("/thursday-notes/2016-06-09"); }
        [Route("/2016/06/16/thursday-notes-6-16-16/")] public IActionResult Notes20160616Redirect() { return RedirectPermanent("/thursday-notes/2016-06-16"); }
        [Route("/2016/09/09/thursday-notes-09-08-16/")] public IActionResult Notes20160908Redirect() { return RedirectPermanent("/thursday-notes/2016-09-08"); }
        [Route("/2016/09/16/thursday-notes-09-15-15/")] public IActionResult Notes20160915Redirect() { return RedirectPermanent("/thursday-notes/2016-09-15"); }
        [Route("/2016/09/22/thursday-notes-09-22-16/")] public IActionResult Notes20160922Redirect() { return RedirectPermanent("/thursday-notes/2016-09-22"); }
        [Route("/2016/09/30/thursday-notes-09-29-16/")] public IActionResult Notes20160929Redirect() { return RedirectPermanent("/thursday-notes/2016-09-29"); }
        [Route("/2016/10/07/thursday-note-10-06-16/")] public IActionResult Notes20161006Redirect() { return RedirectPermanent("/thursday-notes/2016-10-06"); }
        [Route("/2016/10/14/thursday-notes-10-13-16/")] public IActionResult Notes20161013Redirect() { return RedirectPermanent("/thursday-notes/2016-10-13"); }
        [Route("/2016/10/20/thursday-notes-10-20-16/")] public IActionResult Notes20161020Redirect() { return RedirectPermanent("/thursday-notes/2016-10-20"); }
        [Route("/2016/10/28/thursday-notes-10-27-16/")] public IActionResult Notes20161027Redirect() { return RedirectPermanent("/thursday-notes/2016-10-27"); }
        [Route("/2016/11/11/thursday-notes-1110-16/")] public IActionResult Notes20161110Redirect() { return RedirectPermanent("/thursday-notes/2016-11-10"); }
        [Route("/2016/11/18/thursday-notes-11-17-16/")] public IActionResult Notes20161117Redirect() { return RedirectPermanent("/thursday-notes/2016-11-17"); }
        [Route("/2016/12/01/thursday-notes-12-1-16/")] public IActionResult Notes20161201Redirect() { return RedirectPermanent("/thursday-notes/2016-12-01"); }
        [Route("/2016/12/09/2344/")] public IActionResult Notes20161208Redirect() { return RedirectPermanent("/thursday-notes/2016-12-08"); }
        [Route("/2016/12/21/thursday-notes-12-22-16/")] public IActionResult Notes20161222Redirect() { return RedirectPermanent("/thursday-notes/2016-12-22"); }
        [Route("/2017/01/12/thursday-notes-1-12-17/")] public IActionResult Notes20170112Redirect() { return RedirectPermanent("/thursday-notes/2017-01-12"); }
        [Route("/2017/01/19/thursday-notes-1-19-17/")] public IActionResult Notes20170119Redirect() { return RedirectPermanent("/thursday-notes/2017-01-19"); }
        [Route("/2017/01/27/thursday-notes-1-26-17/")] public IActionResult Notes20170126Redirect() { return RedirectPermanent("/thursday-notes/2017-01-26"); }
        [Route("/2017/02/02/thursday-notes-2-2-17/")] public IActionResult Notes20170202Redirect() { return RedirectPermanent("/thursday-notes/2017-02-02"); }
        [Route("/2017/02/10/thursday-notes-2-9-17/")] public IActionResult Notes20170209Redirect() { return RedirectPermanent("/thursday-notes/2017-02-09"); }
        [Route("/2017/02/17/thursday-notes-2-16-17/")] public IActionResult Notes20170216Redirect() { return RedirectPermanent("/thursday-notes/2017-02-16"); }
        [Route("/2017/02/23/thursday-notes-2-23-17/")] public IActionResult Notes20170223Redirect() { return RedirectPermanent("/thursday-notes/2017-02-23"); }
        [Route("/2017/03/05/thursday-notes-3-2-17/")] public IActionResult Notes20170302Redirect() { return RedirectPermanent("/thursday-notes/2017-03-02"); }
        [Route("/2017/03/10/thursday-notes-3-9-17/")] public IActionResult Notes20170309Redirect() { return RedirectPermanent("/thursday-notes/2017-03-09"); }
        [Route("/2017/03/24/thursday-notes-3-23-17/")] public IActionResult Notes20170323Redirect() { return RedirectPermanent("/thursday-notes/2017-03-23"); }
        [Route("/2017/03/30/thursday-notes-3-30-17/")] public IActionResult Notes20170330Redirect() { return RedirectPermanent("/thursday-notes/2017-03-30"); }
        [Route("/2017/04/13/thursday-notes-4-13-17/")] public IActionResult Notes20170413Redirect() { return RedirectPermanent("/thursday-notes/2017-04-13"); }
        [Route("/2017/04/20/thursday-notes-4-20-17/")] public IActionResult Notes20170420Redirect() { return RedirectPermanent("/thursday-notes/2017-04-20"); }
        [Route("/2017/04/27/thursday-notes-4-27-17/")] public IActionResult Notes20170427Redirect() { return RedirectPermanent("/thursday-notes/2017-04-27"); }
        [Route("/2017/05/05/thursday-notes-5-4-17/")] public IActionResult Notes20170504Redirect() { return RedirectPermanent("/thursday-notes/2017-05-04"); }
        [Route("/2017/05/11/thursday-notes-5-11-17/")] public IActionResult Notes20170511Redirect() { return RedirectPermanent("/thursday-notes/2017-05-11"); }
        [Route("/2017/06/01/thursday-notes-6-1-17/")] public IActionResult Notes20170601Redirect() { return RedirectPermanent("/thursday-notes/2017-06-01"); }


        #endregion
    }
}
