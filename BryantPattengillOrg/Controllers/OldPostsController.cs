using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BryantPattengillOrg.Models;
using Microsoft.AspNetCore.Hosting;
using BryantPattengillOrg.Services;
using BryantPattengillOrg.Models.Board;
using BryantPattengillOrg.Models.Events;

namespace BryantPattengillOrg.Controllers
{
    public class OldPostsController : Controller
    {
        private readonly IHostingEnvironment _Env;
        private readonly IPageDataService _PageData;

        public OldPostsController(
           IHostingEnvironment env, IPageDataService pageData)
        {
            _Env = env;
            _PageData = pageData;
        }

        [Route("/Archive")]
        public IActionResult Index()
        {
            return View();
        }

        [Route("/{year}/{month}/{day}/{id}")]
        public IActionResult Detail(string id)
        {
            var page = (from p in _PageData.OldPosts
                        where p.PostName == id
                        select p).FirstOrDefault();

            if (page == null)
            {
                return NotFound();
            }

            ViewData["DisplayDate"] = page.ConvertedDateTime.ToString("MMMM dd, yyyy");
            ViewData["Created"] = page.PostDate;
            ViewData["CreatedGMT"] = page.PostDateGMT;
            ViewData["Title"] = page.Title;
            ViewData["OriginalId"] = page.PostName;
            ViewData["OriginalLink"] = page.Link;
            ViewData["Category"] = page.Category;
            ViewData["PostType"] = page.PostType;
            ViewData["Status"] = page.Status;

            return View(page);
        }
    }
}


