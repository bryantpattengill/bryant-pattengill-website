﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BryantPattengillOrg.Models;
using Microsoft.AspNetCore.Hosting;
using BryantPattengillOrg.Services;
using BryantPattengillOrg.Models.Board;
using BryantPattengillOrg.Models.Events;

namespace BryantPattengillOrg.Controllers
{
    public class SchoolsController : Controller
    {
        private readonly IHostingEnvironment _Env;
        private readonly IPageDataService _PageData;
        
        public SchoolsController(
           IHostingEnvironment env, IPageDataService pageData)
        {
            _Env = env;
            _PageData = pageData;
        }

       
        [Route("/schools")]
        public IActionResult Index()
        {
            
            return View();
        }

        [Route("/schools/useful-resources")]
        public IActionResult UsefulResourcesRedirect()
        {
            return RedirectPermanent("/schools");
        }

        [Route("/student-directory")]
        public IActionResult StudentDirectortyRedirect()
        {
            return RedirectPermanent("/schools");
        }
        

        [Route("/schools/bryant")]
        public IActionResult Bryant()
        {
            var events = _PageData.GetUpcomingEvents(5, EventType.Bryant);

            //if ((events == null) || (events.Count > 0))
            //{
            //    return NotFound();
            //}

            var model = new SchoolDetailViewModel();
            model.UpcomingEvents = events;

            return View(model);
        }

        [Route("/schools/pattengill")]
        public IActionResult Pattengill()
        {
            var events = _PageData.GetUpcomingEvents(5, EventType.Pattengill);

            
            //if ((events == null) || (events.Count > 0))
            //{
            //    return NotFound();
            //}

            var model = new SchoolDetailViewModel();
            model.UpcomingEvents = events;

            return View(model);
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
