﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BryantPattengillOrg.Models;
using BryantPattengillOrg.Services;

namespace BryantPattengillOrg.Controllers
{
    public class HomeController : Controller
    {
        private readonly IPageDataService _PageData;

        public HomeController(IPageDataService pageData)
        {
            _PageData = pageData;
        }

        public IActionResult Index()
        {
            var events = _PageData.GetUpcomingEvents(8);

            
            //var nextBoardMeeting = (from e in _PageData.Events
            //                        where today < e.Start && e.Category == Models.Events.CategoryType.PTOBoardMeeting
            //                        select e).Take(1).FirstOrDefault();
            //var nextGeneralMeeting = (from e in _PageData.Events
            //                          where today < e.Start && e.Category == Models.Events.CategoryType.PTOGeneralMeeting
            //                          select e).Take(1).FirstOrDefault();

            var model = new HomeIndexViewModel();
            model.UpcomingEvents = events;
            //model.NextGeneralMeeting = nextGeneralMeeting;
            //model.NextBoardMeeting = nextBoardMeeting;



            return View(model);
        }

        

        [Route("/about")]
        public IActionResult About()
        {
            return View();
        }

        [Route("/contact")]
        public IActionResult Contact()
        {
            return View();
        }

        [Route("/contact/form")]
        public IActionResult ContactFormRedirect()
        {
            return RedirectPermanent("/contact");
        }

        [Route("/contact/submit-teacher-profile")]
        public IActionResult SubmitTeacherProfileRedirect()
        {
            return RedirectPermanent("/contact");
        }

        [Route("/support")]
        public IActionResult Support()
        {
            return View();
        }

        

        [Route("/volunteer")]
        [Route("/volunteers")]
        public IActionResult Volunteer()
        {
            return View();
        }

        [Route("/donate/thankyou")]
        public IActionResult DonateThankYou()
        {
            return View();
        }
        [Route("/international-families")]
        public IActionResult InternationalFamilies()
        {
            return View();
        }

        [Route("/how-to-use-square")]
        public IActionResult HowToUseSquare()
        {
            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
