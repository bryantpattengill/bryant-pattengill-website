﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BryantPattengillOrg.Models;
using Microsoft.AspNetCore.Hosting;
using BryantPattengillOrg.Services;
using BryantPattengillOrg.Models.Board;
using BryantPattengillOrg.Models.Events;

namespace BryantPattengillOrg.Controllers
{
    public class BoardController : Controller
    {
        private readonly IHostingEnvironment _Env;
        private readonly IPageDataService _PageData;
        
        public BoardController(
           IHostingEnvironment env, IPageDataService pageData)
        {
            _Env = env;
            _PageData = pageData;
        }

        [Route("/board")]
        public IActionResult Index()
        {
            var model = new BoardIndexViewModel();
            model.GeneralMeetings = _PageData.Events.Where(x => x.Category == CategoryType.PTOGeneralMeeting).ToList();
            model.BoardMeetings = _PageData.Events.Where(x => x.Category == CategoryType.PTOBoardMeeting).ToList();

            return View(model);
        }


        [Route("/room-parents")]
        public IActionResult RoomParents()
        {
            
            return View();
        }

        [Route("/board/current-members/")]
        public IActionResult CurrentMembersRedirect()
        {
            return RedirectPermanent("/board#current-members");
        }

        [Route("/board/meetings/")]
        public IActionResult Meetings()
        {
            var model = new BoardIndexViewModel();
            model.GeneralMeetings = _PageData.Events.Where(x => x.Category == CategoryType.PTOGeneralMeeting).ToList();
            model.BoardMeetings = _PageData.Events.Where(x => x.Category == CategoryType.PTOBoardMeeting).ToList();

            return View(model);
        }

        [Route("/board/bylaws/")]
        public IActionResult ByLawsRedirect()
        {
            return RedirectPermanent("/board#bylaws");
        }

        [Route("/board/bylaws/")]
        public IActionResult BudgetRedirect()
        {
            return RedirectPermanent("/board#budget");
        }

        [Route("/board/forms/")]
        public IActionResult FormsRedirect()
        {
            return RedirectPermanent("/board#forms");
        }

    
        
        [Route("/board/current-members/{id}")]
        public IActionResult CurrentMember(string id)
        {
            return RedirectPermanent($"/board/members/{id}");
        }

        [Route("/board/members/{id}")]
        public IActionResult Member(string id)
        {
            var member = (from m in _PageData.BoardMembers
                                 where m.Id == id.ToLower()
                                 select m).FirstOrDefault();
            if (member == null)
            {
                return NotFound();
            }
            return View(member);
        }
        

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
