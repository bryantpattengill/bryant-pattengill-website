﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BryantPattengillOrg.Models;
using Microsoft.AspNetCore.Hosting;
using BryantPattengillOrg.Services;
using BryantPattengillOrg.Models.Board;
using BryantPattengillOrg.Models.Events;

namespace BryantPattengillOrg.Controllers
{
    public class EventsController : Controller
    {
        private readonly IHostingEnvironment _Env;
        private readonly IPageDataService _PageData;
        
        public EventsController(
           IHostingEnvironment env, IPageDataService pageData)
        {
            _Env = env;
            _PageData = pageData;
        }

       
        [Route("/events")]
        public IActionResult Index()
        {
            var events = _PageData.GetUpcomingEvents();
          

            //if ((events == null) || (events.Count > 0))
            //{
            //    return NotFound();
            //}
            return View(events);
        }

        [Route("/events/calendars/")]
        public IActionResult CalendarsRedirect()
        {
            return RedirectPermanent("/events#calendar");
        }

        #region International Festival
        [Route("/ifest")]
        [Route("/events/ifest")]
        [Route("/international-festival")]
        public IActionResult InternationalFestivalRedirect()
        {
            return RedirectPermanent("/events/international-festival");
        }

        [Route("/events/international-festival")]
        public IActionResult InternationalFestival()
        {
            return View();
        }
        #endregion

        #region Girl Scouts
        [Route("/girl-scouts")]
        public IActionResult GirlScoutsRedirect()
        {
            return RedirectPermanent("/events/girl-scouts");
        }

        [Route("/events/girl-scouts")]
        public IActionResult GirlScouts()
        {
            return View();
        }
        #endregion

        #region 5th Grade Camp
        [Route("/5th-grade-camp")]
        public IActionResult FifthGradeCampRedirect()
        {
            return RedirectPermanent("/events/5th-grade-camp");
        }

        [Route("/events/5th-grade-camp")]
        public IActionResult FifthGradeCamp()
        {
            return View();
        }

        [Route("/events/5th-grade-camp/thank-you")]
        public IActionResult FifthGradeCampThankYou()
        {
            return View();
        }
        #endregion

        #region Tech Night
        [Route("/tech-night")]
        public IActionResult TechNightRedirect()
        {
            return RedirectPermanent("/events/tech-night");
        }

        [Route("/events/tech-night")]
        public IActionResult TechNight()
        {
            return View();
        }
        #endregion

        #region Ice Cream Social
        [Route("/ice-cream-social")]
        public IActionResult IceCreamSocialRedirect()
        {
            return RedirectPermanent("/events/ice-cream-social");
        }

        [Route("/ics")]
        public IActionResult IceCreamSocialShortRedirect()
        {
            return RedirectPermanent("/events/ice-cream-social");
        }

        [Route("/events/ice-cream-social")]
        public IActionResult IceCreamSocial()
        {
            return View();
        }
        #endregion

        #region Movie Night

        [Route("/movie-night")]
        public IActionResult MovieNightRedirect()
        {
            return RedirectPermanent("/events/movie-night");
        }

        [Route("/events/movie-night")]
        public IActionResult MovieNight()
        {
            return View();
        }

        #endregion
        
        #region Academic Games
        [Route("/academic-games")]
        public IActionResult AcademicGamesRedirect()
        {
            return RedirectPermanent("/events/academic-games");
        }
        [Route("/events/academic-games")]
        public IActionResult AcademicGames()
        {
            return View();
        }
        #endregion

        #region Parent Lending Library

        [Route("/parent-lending-library")]
        public IActionResult ParentLendingLibraryRedirect()
        {
            return RedirectPermanent("/events/parent-lending-library");
        }

        [Route("/events/parent-lending-library")]
        public IActionResult ParentLendingLibrary()
        {
            return View();
        }
        #endregion

        #region Read a thon
        [Route("/read-a-thon")]
        [Route("/readathon")]
        [Route("/rat")]
        public IActionResult ReadAThonRedirect()
        {
            return RedirectPermanent("/events/read-a-thon");
        }

        [Route("/events/read-a-thon")]
        public IActionResult ReadAThon()
        {
            return View();
        }

        
        #endregion

        [Route("/events/upcoming-events/")]
        public IActionResult UpcomingEventsRedirect()
        {
            return RedirectPermanent("/events#upcoming");
        }

        #region Science Olympiad
        [Route("/science-olympiad")]
        [Route("/so/")]
        [Route("/events/so/")]
        public IActionResult ScienceOlympiadRedirect()
        {
            return RedirectPermanent("/events/science-olympiad");
        }

        [Route("/history-of-weso/")]
        [Route("/2016/05/10/history-of-weso/")]
        public IActionResult ScienceOlympiadHistoryRedirect()
        {
            return RedirectPermanent("/events/science-olympiad/history-of-weso");
        }

        [Route("/events/science-olympiad/history-of-weso")]
        public IActionResult ScienceOlympiadHistory()
        {
            return View();
        }

        [Route("/events/science-olympiad")]
        public IActionResult ScienceOlympiad()
        {
            return View();
        }

        [Route("/events/so/topics")]
        public IActionResult ScienceOlympiadTopicsRedirect()
        {
            return RedirectPermanent("/events/science-olympiad");
        }

        #endregion

        //[Route("/events/current-members/{id}")]
        //public IActionResult CurrentMember(string id)
        //{
        //    return RedirectPermanent($"/board/members/{id}");
        //}

        //[Route("/events/{id}")]
        //public IActionResult Member(string id)
        //{
        //    var member = (from m in _PageData.BoardMembers
        //                         where m.Id == id.ToLower()
        //                         select m).FirstOrDefault();
        //    if (member == null)
        //    {
        //        return NotFound();
        //    }
        //    return View(member);
        //}


        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
