﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using BryantPattengillOrg.Services;
using Newtonsoft.Json;

namespace BryantPattengillOrg
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => false;
                options.MinimumSameSitePolicy = Microsoft.AspNetCore.Http.SameSiteMode.None;
            });

            services.AddSingleton<IPageDataService, PageDataService>();

            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IPageDataService pageData)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();

            var dataFilePath = $@"{env.ContentRootPath}/DataFiles/";

            pageData.LoadBoardMembers($"{dataFilePath}boardmembers.json");
            pageData.LoadEvents($"{dataFilePath}events.json");

            //pageData.LoadOldNotes($"{dataFilePath}old-notes.json");
            //pageData.LoadOldPages($"{dataFilePath}old-pages.json");
            pageData.LoadOldPosts($"{dataFilePath}old-posts.json");

            app.UseStaticFiles();

            app.UseCookiePolicy();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            // generate data
            //var eventsData = pageData.Events.OrderBy(x => x.Start);
            //var eventsJsonData = JsonConvert.SerializeObject(eventsData);
            //System.IO.File.WriteAllText($@"{dataFilePath}events.json", eventsJsonData);

            //var boardMembersData = pageData.BoardMembers.OrderBy(x => x.Id);
            //var boardMembersJsonData = JsonConvert.SerializeObject(boardMembersData);
            //System.IO.File.WriteAllText($@"{dataFilePath}boardmembers.json", boardMembersJsonData);

        }
    }
}
