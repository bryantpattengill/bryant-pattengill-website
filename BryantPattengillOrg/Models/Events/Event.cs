﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BryantPattengillOrg.Models;

namespace BryantPattengillOrg.Models.Events
{
    public class Event
    {
        //public string Id { get; set; }

        [JsonProperty(PropertyName = "Title", NullValueHandling = NullValueHandling.Ignore)]
        public string Title { get; set; }

        [JsonProperty(PropertyName = "Start", NullValueHandling = NullValueHandling.Ignore)]
        public DateTime Start { get; set; }

        [JsonProperty(PropertyName = "End", NullValueHandling = NullValueHandling.Ignore)]
        public DateTime? End { get; set; }

        [JsonProperty(PropertyName = "Type", NullValueHandling = NullValueHandling.Ignore)]
        public EventType Type { get; set; }

        [JsonProperty(PropertyName = "Category", NullValueHandling = NullValueHandling.Ignore)]
        public CategoryType Category { get; set; }

        [JsonProperty(PropertyName = "IsAllDay", NullValueHandling = NullValueHandling.Ignore)]
        public bool IsAllDay { get; set; }

        [JsonProperty(PropertyName = "ShortDescription", NullValueHandling = NullValueHandling.Ignore)]
        public string ShortDescription { get; set; }

        [JsonIgnore]
        public bool IsShortDescription
        {
            get
            {
                return !string.IsNullOrWhiteSpace(ShortDescription);
            }
        }

        [JsonProperty(PropertyName = "FullDescription", NullValueHandling = NullValueHandling.Ignore)]
        public string FullDescription { get; set; }

        [JsonProperty(PropertyName = "Location", NullValueHandling = NullValueHandling.Ignore)]
        public string Location { get; set; }

        [JsonIgnore]
        public bool IsLocation
        {
            get
            {
                return !string.IsNullOrWhiteSpace(Location);
            }
        }

        [JsonProperty(PropertyName = "Url", NullValueHandling = NullValueHandling.Ignore)]
        public string Url { get; set; }

        [JsonIgnore]
        public bool IsUrl
        {
            get
            {
                return !string.IsNullOrWhiteSpace(Url);
            }
        }
        
        [JsonIgnore]
        public bool IsTimeRange
        {
            get
            {
                return !string.IsNullOrWhiteSpace(TimeRange);
            }
        }

        [JsonIgnore]
        public string TimeRange
        {
            get
            {
                if (IsAllDay)
                {
                    return String.Empty;
                }

                if ((End.HasValue)
                    && (Start.Year == End.Value.Year)
                    && (Start.Month == End.Value.Month)
                    && (Start.ToTimeZoneTime().Day == End.Value.ToTimeZoneTime().Day))
                {
                    //return $"{Start.ToString("h:mm")}-{End.Value.ToString("h:mm tt")}";
                   return $"{Start.ToTimeZoneTime().ToString("h:mm")}-{End.Value.ToTimeZoneTime().ToString("h:mm tt")}";
                }
                else
                {
                    return Start.ToTimeZoneTime().ToString("h:mm tt");
                    //return Start.ToString("h:mm tt");
                }
            }
            
        }

        [JsonIgnore]
        public string DateRange
        {
            get
            {
                if ( End.HasValue 
                    && (
                    (Start.ToTimeZoneTime().Year == End.Value.ToTimeZoneTime().Year)
                    && (Start.ToTimeZoneTime().Month == End.Value.ToTimeZoneTime().Month)
                    && (Start.ToTimeZoneTime().Day != End.Value.ToTimeZoneTime().Day)
                    ))
                {
                    return $"{Start.ToTimeZoneTime().ToString("MMM d")}{End.Value.ToTimeZoneTime().ToString("-d")}";
                }
                else if (End.HasValue
                    && (Start.ToTimeZoneTime().Month != End.Value.ToTimeZoneTime().Month)
                    && (Start.ToTimeZoneTime().Day != End.Value.ToTimeZoneTime().Day)
                    )
                {
                   return $"{Start.ToTimeZoneTime().ToString("MMM d")}-{End.Value.ToTimeZoneTime().ToString("MMM d")}";
                }
                else
                {
                    return Start.ToTimeZoneTime().ToString("MMM d");
                }
            }
           
        }

        [JsonIgnore]
        public string WeekDayRange
        {
            get
            {
                if ((End.HasValue)
                    &&
                    ((Start.ToTimeZoneTime().Year != End.Value.ToTimeZoneTime().Year)
                    || (Start.ToTimeZoneTime().Month != End.Value.ToTimeZoneTime().Month)
                    || (Start.ToTimeZoneTime().Day != End.Value.ToTimeZoneTime().Day)
                    ))
                {
                    return $"{Start.ToTimeZoneTime().ToString("ddd")}-{End.Value.ToTimeZoneTime().ToString("ddd")}";
                }
                else
                {
                    return Start.ToTimeZoneTime().ToString("ddd");
                }
            }

        }
    }



    public enum EventType
    {
        Unknown  = 0,
        BryantAndPattengill = 1,
        Bryant = 2,
        Pattengill = 3
    }


    public enum CategoryType
    {
        Unknown = 0,
        PTOGeneralMeeting = 1,
        PTOBoardMeeting = 2,
        EarlyRelease = 3,
        NoSchool = 4,
        Fundraiser = 5,
        FieldDay = 6,
        Ceremony = 7,
        Informational = 8,
        Concert = 9,
        Social = 10,
        CurriculumNight = 11,
        PictureDay = 12,
        SITMeeting = 13,
        CountDay = 14,
        FieldTrip = 15,
        BookFair = 16,
        TechNight = 17,
        Assembly = 18,
        TalentShow = 19,
        ScienceNight = 20,
        ScienceOlympiad = 21,
        Naapid = 22,
        ScienceFair = 23,
        KYoung5RoundUp = 24,
        ReadAThon = 25,
        Registration = 26,
        VolunteerAppreciation = 27,
        ServiceDay = 28,
        InternationalFestival = 29,
        TransitionMeeting = 30,
        Workshop = 31,
        RestaurantDay = 32,
        AcademicGames = 33,
        ParentLendingLibrary = 34,
        GirlScouts = 35,
        BoxTops = 36,
        CommunityMeeting = 37,
        ThursdayNotes = 38,
        Dentist = 39
        

    }
}
