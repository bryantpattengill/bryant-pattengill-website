﻿using BryantPattengillOrg.Models.Board;
using BryantPattengillOrg.Models.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BryantPattengillOrg.Models
{
    public class HomeIndexViewModel
    {
        public List<Event> UpcomingEvents { get; set; }
        public Event NextGeneralMeeting { get; set; }
        public Event NextBoardMeeting { get; set; }

        //public List<BoardMember> BoardMembers { get; set; }

    }
}
