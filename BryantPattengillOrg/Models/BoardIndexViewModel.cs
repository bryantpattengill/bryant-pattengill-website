﻿using BryantPattengillOrg.Models.Board;
using BryantPattengillOrg.Models.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BryantPattengillOrg.Models
{
    public class BoardIndexViewModel
    {
        public List<Event> Meetings { get; set; }
        public List<Event> GeneralMeetings { get; set; }
        public List<Event> BoardMeetings { get; set; }

        public List<BoardMember> BoardMembers { get; set; }

    }
}
