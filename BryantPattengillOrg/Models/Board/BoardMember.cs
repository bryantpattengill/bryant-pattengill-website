﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BryantPattengillOrg.Models.Board
{
    public class BoardMember
    {
        [JsonProperty(PropertyName = "Id", NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "Name", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "Image", NullValueHandling = NullValueHandling.Ignore)]
        public string Image { get; set; }

        [JsonIgnore]
        public bool IsImage
        {
            get
            {
                return !string.IsNullOrWhiteSpace(Image);
            }
        }

        [JsonProperty(PropertyName = "Positions", NullValueHandling = NullValueHandling.Ignore)]
        public List<string> Positions { get; set; }

        [JsonIgnore]
        public bool IsPositions
        {
            get
            {
                return Positions != null && Positions.Count > 0;
            }
        }

        [JsonIgnore]
        public bool IsContactInformation
        {
            get
            {
                return IsEmail || IsFacebook || IsLinkedIn || IsPhone || IsTwitter || IsWebsite;
            }
        }

        [JsonProperty(PropertyName = "Phone", NullValueHandling = NullValueHandling.Ignore)]
        public List<string> Phone { get; set; }

        [JsonIgnore]
        public bool IsPhone
        {
            get
            {
                return Phone != null && Phone.Count > 0;
            }
        }

        [JsonProperty(PropertyName = "Email", NullValueHandling = NullValueHandling.Ignore)]
        public List<string> Email { get; set; }

        [JsonIgnore]
        public bool IsEmail
        {
            get
            {
                return Email != null && Email.Count > 0;
            }
        }

        [JsonProperty(PropertyName = "Website", NullValueHandling = NullValueHandling.Ignore)]
        public string Website { get; set; }

        [JsonIgnore]
        public bool IsWebsite
        {
            get
            {
                return !string.IsNullOrWhiteSpace(Website);
            }
        }

        [JsonProperty(PropertyName = "Twitter", NullValueHandling = NullValueHandling.Ignore)]
        public string Twitter { get; set; }

        [JsonIgnore]
        public bool IsTwitter
        {
            get
            {
                return !string.IsNullOrWhiteSpace(Twitter);
            }
        }

        [JsonProperty(PropertyName = "LinkedIn", NullValueHandling = NullValueHandling.Ignore)]
        public string LinkedIn { get; set; }

        [JsonIgnore]
        public bool IsLinkedIn
        {
            get
            {
                return !string.IsNullOrWhiteSpace(LinkedIn);
            }
        }

        [JsonProperty(PropertyName = "Facebook", NullValueHandling = NullValueHandling.Ignore)]
        public string Facebook { get; set; }

        [JsonIgnore]
        public bool IsFacebook
        {
            get
            {
                return !string.IsNullOrWhiteSpace(Facebook);
            }
        }

        [JsonProperty(PropertyName = "IsCurrent", NullValueHandling = NullValueHandling.Ignore)]
        public bool IsCurrent { get; set; }

    }

}
