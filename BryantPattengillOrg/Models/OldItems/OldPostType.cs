﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BryantPattengillOrg.Models
{
    public enum OldPostType
    {
        Unknown = 1,
        Post = 2,
        Page = 3,
        ThursdayNotes = 4
    }
}
