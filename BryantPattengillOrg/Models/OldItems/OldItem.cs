﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BryantPattengillOrg.Models
{
    public class OldItem
    {
        [JsonProperty(PropertyName = "t", NullValueHandling = NullValueHandling.Ignore)]
        public string Title { get; set; }

        [JsonProperty(PropertyName = "l", NullValueHandling = NullValueHandling.Ignore)]
        public string Link { get; set; }

        [JsonProperty(PropertyName = "d3", NullValueHandling = NullValueHandling.Ignore)]
        public string PubDate { get; set; }

        [JsonIgnore]
        public string Guid { get; set; }

        [JsonProperty(PropertyName = "c", NullValueHandling = NullValueHandling.Ignore)]
        public string Content { get; set; }

        [JsonIgnore]
        public DateTime ConvertedDateTime
        {
            get
            {
                return DateTime.Parse(PostDate);
            }
        }

        [JsonProperty(PropertyName = "d", NullValueHandling = NullValueHandling.Ignore)]
        public string PostDate { get; set; }

        [JsonProperty(PropertyName = "d2", NullValueHandling = NullValueHandling.Ignore)]
        public string PostDateGMT { get; set; }

        [JsonProperty(PropertyName = "n", NullValueHandling = NullValueHandling.Ignore)]
        public string PostName { get; set; }

        [JsonIgnore]
        public string PostType { get; set; }

        [JsonProperty(PropertyName = "s", NullValueHandling = NullValueHandling.Ignore)]
        public string Status { get; set; }

        [JsonProperty(PropertyName = "id", NullValueHandling = NullValueHandling.Ignore)]
        public string PostId { get; set; }

        [JsonProperty(PropertyName = "cat", NullValueHandling = NullValueHandling.Ignore)]
        public string Category { get; set; }

        [JsonProperty(PropertyName = "type", NullValueHandling = NullValueHandling.Ignore)]
        public OldPostType Type { get; set; }
    }

    
}
