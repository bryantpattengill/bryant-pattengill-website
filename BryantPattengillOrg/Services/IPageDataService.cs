﻿using BryantPattengillOrg.Models;
using BryantPattengillOrg.Models.Board;
using BryantPattengillOrg.Models.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BryantPattengillOrg.Services
{
    public interface IPageDataService
    {
        //List<OldItem> OldNotes { get; set; }
        //List<OldItem> OldPages { get; set; }
        List<OldItem> OldPosts { get; set; }

        List<BoardMember> BoardMembers { get; set; }
        List<Event> Events { get; set; }

        void LoadBoardMembers(string jsonFile);
        void LoadEvents(string jsonFile);

        void LoadOldPosts(string jsonFile);
        //void LoadOldNotes(string jsonFile);
        //void LoadOldPages(string jsonFile);

        List<Event> GetUpcomingEvents(int? itemLimit = null, EventType? schoolFilter = EventType.BryantAndPattengill);


    }

}
