﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BryantPattengillOrg.Models.Board;
using BryantPattengillOrg.Models.Events;
using Newtonsoft.Json;
using System.IO;
using BryantPattengillOrg.Models;

namespace BryantPattengillOrg.Services
{
    public class PageDataService : IPageDataService
    {

        public List<OldItem> OldNotes { get; set; }
        public List<OldItem> OldPages { get; set; }
        public List<OldItem> OldPosts { get; set; }

        public void LoadOldPosts(string jsonFile)
        {
            string rawString = File.ReadAllText(jsonFile);

            OldPosts = JsonConvert.DeserializeObject<List<OldItem>>(rawString);
        }

        public void LoadOldNotes(string jsonFile)
        {
            string rawString = File.ReadAllText(jsonFile);

            OldNotes = JsonConvert.DeserializeObject<List<OldItem>>(rawString);
        }

        public void LoadOldPages(string jsonFile)
        {
            string rawString = File.ReadAllText(jsonFile);

            OldPages = JsonConvert.DeserializeObject<List<OldItem>>(rawString);
        }

        public List<BoardMember> BoardMembers { get; set; }
        public List<Event> Events { get; set; }

        public List<Event> GetUpcomingEvents(int? itemLimit = null, EventType? schoolFilter = EventType.BryantAndPattengill)
        {
            var tomorrow = DateTime.Today - TimeSpan.FromDays(1);

            var query = (from e in Events
                         where tomorrow < e.End || (tomorrow < e.Start && e.IsAllDay)
                         select e).AsQueryable();

            if (schoolFilter != EventType.BryantAndPattengill)
            {
                query = query.Where(x => x.Type == schoolFilter || x.Type == EventType.BryantAndPattengill);
            }

            if (itemLimit.HasValue)
            {
                query = query.Take(itemLimit.Value);
            }

            return query.ToList();
        }


        public void LoadBoardMembers(string jsonFile)
        {
            string rawString = File.ReadAllText(jsonFile);

            BoardMembers = JsonConvert.DeserializeObject<List<BoardMember>>(rawString);

            #region Old Data
            //var members = new List<BoardMember>();

            //members.Add(new BoardMember()
            //{
            //    Id = "kathy-brady",
            //    Name = "Kathy Brady",
            //    IsCurrent = false,
            //    Phone = new List<string>() { "(734) 389-3459" },
            //    Email = new List<string>() { "kathy.brady@bryantpattengill.org" },
            //    Positions = new List<string>() { "Co-President 2017/18", "Co-President 2016/17", "Co-President 2015/16", "Secretary 2014/15" }
            //});
            //members.Add(new BoardMember()
            //{
            //    Id = "ariel-hurwitz-greene",
            //    Name = "Ariel Hurwitz-Greene",
            //    IsCurrent = true,
            //    Phone = new List<string>() { "(734) 646-5333" },
            //    Email = new List<string>() { "president@bryantpattengill.org", "ariel.hurwitz-greene@bryantpattengill.org" },
            //    Positions = new List<string>() { "Co-President 2018/19", "Co-President 2017/18", "External Secretary 2016/17", "Secretary 2015/16" }
            //});
            //members.Add(new BoardMember()
            //{
            //    Id = "jen-teeter",
            //    Name = "Jen Teeter",
            //    IsCurrent = true,
            //    Phone = new List<string>() { "(734) 604-6369" },
            //    Email = new List<string>() { "president@bryantpattengill.org", "jen.teeter@bryantpattengill.org" },
            //    Positions = new List<string>() { "Co-President 2018/19", "Vice President, Bryant 2017/18", "Internal Secretary 2016/17" },
            //    Facebook = "JenHastings",
            //    Image = "/images/profile/jen-teeter.jpg"
            //});

            //members.Add(new BoardMember()
            //{
            //    Id = "karishma-collette",
            //    Name = "Karishma Collette",
            //    IsCurrent = true,
            //    //Phone = new List<string>() { "" },
            //    Email = new List<string>() { "vpbryant@bryantpattengill.org", "karishma.collette@bryantpattengill.org" },
            //    Positions = new List<string>() { "Vice President, Bryant 2018/19" }
            //});
            //members.Add(new BoardMember()
            //{
            //    Id = "maggie-shee",
            //    Name = "Maggie Shee",
            //    IsCurrent = true,
            //    Phone = new List<string>() { "(646) 387-7100" },
            //    Email = new List<string>() { "vppattengill@bryantpattengill.org", "maggie.shee@bryantpattengill.org" },
            //    Positions = new List<string>() { "Vice President, Pattengill 2018/19", "Vice President, Pattengill 2017/18", "Vice President, Pattengill 2016/17" }
            //});
            //members.Add(new BoardMember()
            //{
            //    Id = "cathy-martin",
            //    Name = "Cathy Martin",
            //    IsCurrent = true,
            //    Phone = new List<string>() { "(734) 395-5955" },
            //    Email = new List<string>() { "secretary@bryantpattengill.org", "cathy.martin@bryantpattengill.org" },
            //    Positions = new List<string>() { "External Secretary 2018/19", "External Secretary 2017/18" }
            //});
            //members.Add(new BoardMember()
            //{
            //    Id = "anthony-king",
            //    Name = "Anthony King",
            //    IsCurrent = true,
            //    Phone = new List<string>() { "(248) 703-5359" },
            //    Email = new List<string>() { "treasurer@bryantpattengill.org", "anthony.king@bryantpattengill.org", },
            //    Positions = new List<string>() { "Co-Treasurer 2017/18" }
            //});
            //members.Add(new BoardMember()
            //{
            //    Id = "joan-beaupre",
            //    Name = "Joan Beaupre",
            //    IsCurrent = true,
            //    Phone = new List<string>() { "(714) 329-2897" },
            //    Email = new List<string>() { "treasurer@bryantpattengill.org", "joan.beaupre@bryantpattengill.org" },
            //    Positions = new List<string>() { "Treasurer 2018/19" }
            //});
            //members.Add(new BoardMember()
            //{
            //    Id = "lorraine-currie",
            //    Name = "Lorraine Currie",
            //    IsCurrent = true,
            //    Phone = new List<string>() { "(734) 961-8461" },
            //    Email = new List<string>() { "treasurer@bryantpattengill.org", "lorraine.currie@bryantpattengill.org" },
            //    Positions = new List<string>() { "Co-Treasurer 2017/18" }
            //});
            //members.Add(new BoardMember()
            //{
            //    Id = "amanda-shutko",
            //    Name = "Amanda Shutko",
            //    IsCurrent = true,
            //    Phone = new List<string>() { "(734) 717-5338" },
            //    Email = new List<string>() { "fundraising@bryantpattengill.org", "amanda.shutko@bryantpattengill.org" },
            //    Positions = new List<string>() { "Fundraising Coordinator 2018/19", "Fundraising Coordinator 2017/18", "Fundraising Coordinator 2016/17" }
            //});
            //members.Add(new BoardMember()
            //{
            //    Id = "lyonel-milton",
            //    Name = "Lyonel Milton",
            //    IsCurrent = true,
            //    //Phone = new List<string>() { "" },
            //    Email = new List<string>() { "sit@bryantpattengill.org", "lyonel.milton@bryantpattengill.org" },
            //    Positions = new List<string>() { "School Improvement Team (\"SIT\") Representative 2018/19", "PTO Council Representative 2017/18" }
            //}); members.Add(new BoardMember()
            //{
            //    Id = "kristy-kubacki",
            //    Name = "Kristy Kubacki",
            //    IsCurrent = false,
            //    Phone = new List<string>() { "(734) 975-2382" },
            //    Email = new List<string>() { "thriftshoprep@bryantpattengill.org", "kristy.kubacki@bryantpattengill.org" },
            //    Positions = new List<string>() { "PTO Thrift Shop Representative 2017/18", "PTO Thrift Shop Representative 2016/17", "PTO Thrift Shop Representative 2015/16", "PTO Thrift Shop Representative 2014/15", "PTO Thrift Shop Representative 2013/14" }
            //});
            //members.Add(new BoardMember()
            //{
            //    Id = "charlita-daniels",
            //    Name = "Charlita Daniels",
            //    IsCurrent = false,
            //    //Phone = new List<string>() { "" },
            //    Email = new List<string>() { "charlita.daniels@bryantpattengill.org" },
            //    Positions = new List<string>() { "School Improvement Team (\"SIT\") Representative 2017/18", "Vice President, Bryant 2016/17" }
            //});
            //members.Add(new BoardMember()
            //{
            //    Id = "marc-mojica",
            //    Name = "Marc Mojica",
            //    IsCurrent = true,
            //    //Phone = new List<string>() { "" },
            //    Email = new List<string>() { "marc@bryantpattengill.org" },
            //    Positions = new List<string>() { "Technology Director 2017/18", "Technology Director 2016/17", "Technology Director 2015/16", "Technology Director 2014/15" },
            //    //Twitter = "MarcMojica",
            //    //LinkedIn = "MarcMojica",
            //    //Website = "https://marcmojica.com",
            //    //Image = "/images/profile/marc-mojica.jpg"
            //});
            //members.Add(new BoardMember()
            //{
            //    Id = "liliana-grueber",
            //    Name = "Liliana Grueber",
            //    IsCurrent = true,
            //    Phone = new List<string>() { "(313) 623-1778" },
            //    Email = new List<string>() { "liliana.grueber@bryantpattengill.org" },
            //    Positions = new List<string>() { "EL (English Learner) Family Coordinator 2017/18" }
            //});
            //members.Add(new BoardMember()
            //{
            //    Id = "roberta-heyward",
            //    Name = "Roberta Heyward",
            //    IsCurrent = true,
            //    //Phone = new List<string>() { "" },
            //    Email = new List<string>() { "heywardr@aaps.k12.mi.us" },
            //    Positions = new List<string>() { "Bryant Principal 2016/17", "Bryant Principal 2015/16", "Bryant Principal 2014/15", "Bryant Principal 2013/14" }
            //});
            //members.Add(new BoardMember()
            //{
            //    Id = "melita-alston",
            //    Name = "Melita Alston",
            //    IsCurrent = true,
            //    Phone = new List<string>() { "734-994-1961" },
            //    Email = new List<string>() { "principals@bryantpattengill.org", "alston@aaps.k12.mi.us" },
            //    Positions = new List<string>() { "Bryant & Pattengill Senior Principal 2018/19", "Bryant & Pattengill Senior Principal 2018", "Pattengill Principal 2017", "Pattengill Principal 2016/17", "Pattengill Principal 2015/16", "Pattengill Principal 2014/15", "Pattengill Principal 2013/14" },
            //    Image = "/images/profile/melita-alston.jpg"
            //});
            //members.Add(new BoardMember()
            //{
            //    Id = "michelle-kloack",
            //    Name = "Michelle Kloack",
            //    IsCurrent = true,
            //    //Phone = new List<string>() { "" },
            //    Email = new List<string>() { "michelle.kloack@bryantpattengill.org" },
            //    Positions = new List<string>() { "Internal Secretary 2018/19", "Internal Secretary 2017/18" }
            //});
            /////// old members now
            //members.Add(new BoardMember()
            //{
            //    Id = "carmen-andrianopoulos",
            //    Name = "Carmen Andrianopoulos",
            //    IsCurrent = false,
            //    Phone = new List<string>() { "(810) 210-3335" },
            //    Email = new List<string>() { "carmen.andrianopoulos@bryantpattengill.org" },
            //    Positions = new List<string>() { "Treasurer 2016/17", "Treasurer 2015/16", "Treasurer 2014/15", "Co-Treasurer 2013/14" }
            //});
            //members.Add(new BoardMember()
            //{
            //    Id = "carmen-atkins",
            //    Name = "Carmen Atkins",
            //    IsCurrent = false,
            //    Phone = new List<string>() { "(734) 330-9967" },
            //    Email = new List<string>() { "carmen.atkins@bryantpattengill.org" },
            //    Positions = new List<string>() { "Vice President, Bryant 2015/16", "Vice President, Bryant 2014/15" } // need to confirm dates
            //});
            //members.Add(new BoardMember()
            //{
            //    Id = "charlita-daniels",
            //    Name = "Charlita Daniels",
            //    IsCurrent = false,
            //    //Phone = new List<string>() { "" },
            //    Email = new List<string>() { "charlita.daniels@bryantpattengill.org" },
            //    Positions = new List<string>() { "School Improvement Team (\"SIT\") Representative 2017/18", "Vice President, Bryant 2016/17" }
            //});
            //members.Add(new BoardMember()
            //{
            //    Id = "deborah-paul-mcdonald",
            //    Name = "Deborah Paul-McDonald",
            //    IsCurrent = false,
            //    Phone = new List<string>() { "(734) 395-0288" },
            //    Email = new List<string>() { "deborah@bryantpattengill.org" },
            //    Positions = new List<string>() { "Co-President 2014/15", "Vice President, Bryant 2013/14" }
            //});
            //members.Add(new BoardMember()
            //{
            //    Id = "jennifer-nguyen",
            //    Name = "Jennifer Nguyen",
            //    IsCurrent = false,
            //    Phone = new List<string>() { "(734) 678-5182" },
            //    Email = new List<string>() { "jennifer.nguyen@bryantpattengill.org" },
            //    Positions = new List<string>() { "Co-President 2016/17", "Vice President, Pattengill 2015/16", "Internal Secretary 2013/14" }
            //});
            //members.Add(new BoardMember()
            //{
            //    Id = "kelly-pomorski",
            //    Name = "Kelly Pomorski",
            //    IsCurrent = false,
            //    Phone = new List<string>() { "(734) 678-2206" },
            //    Email = new List<string>() { "kelly.pomorski@bryantpattengill.org" },
            //    Positions = new List<string>() { "PTO Board Project Assistant 2016/17", "PTO Board Project Assistant 2015/16", "PTO Board Project Assistant 2014/15", "PTO Board Project Assistant 2013/14" }
            //});
            //members.Add(new BoardMember()
            //{
            //    Id = "lisa-treat",
            //    Name = "Lisa Treat",
            //    IsCurrent = false,
            //    Phone = new List<string>() { "(734) 358-1450" },
            //    Email = new List<string>() { "lisa.treat@bryantpattengill.org" },
            //    Positions = new List<string>() { "Co-President 2015/16", "Co-President 2014/15", "Co-President 2013/14" }
            //});
            //members.Add(new BoardMember()
            //{
            //    Id = "stephanie-carlson",
            //    Name = "Stephanie Carlson",
            //    IsCurrent = false,
            //    Phone = new List<string>() { "(734) 717-2097" },
            //    Email = new List<string>() { "stephanie.carlson@bryantpattengill.org" },
            //    Positions = new List<string>() { "Vice President, Pattengill 2014/15" }
            //});
            //members.Add(new BoardMember()
            //{
            //    Id = "stephen-ward",
            //    Name = "Stephen Ward",
            //    IsCurrent = false,
            //    Phone = new List<string>() { "(734) 320-4198" },
            //    Email = new List<string>() { "stephen.ward@bryantpattengill.org" },
            //    Positions = new List<string>() { "School Improvement Team (\"SIT\") Representative 2016/17", "School Improvement Team (\"SIT\") Representative 2015/16", "School Improvement Team (\"SIT\") Representative 2014/15", "School Improvement Team (\"SIT\") Representative 2013/14" }
            //});
            //members.Add(new BoardMember()
            //{
            //    Id = "robert-treat",
            //    Name = "Robert Treaet",
            //    IsCurrent = false,
            //    Phone = new List<string>() { "(734) 330-8266" },
            //    Email = new List<string>() { "robert.treat@bryantpattengill.org" },
            //    Positions = new List<string>() { "Fundraising Coordinator 2015/16", "Fundraising Coordinator 2014/15", "Fundraising Coordinator 2013/14" }
            //});
            //members.Add(new BoardMember()
            //{
            //    Id = "joy-meadows",
            //    Name = "Joy Meadows",
            //    IsCurrent = false,
            //    Phone = new List<string>() { "" },
            //    Email = new List<string>() { "joy.meadows@bryantpattengill.org" },
            //    Positions = new List<string>() { "PTO Council (\"PTOC\") Representative 2014/15" }
            //});
            //members.Add(new BoardMember()
            //{
            //    Id = "kathryn-powell",
            //    Name = "Kathryn Powell",
            //    IsCurrent = true,
            //    //Phone = new List<string>() { "" },
            //    Email = new List<string>() { "ptoc@bryantpattengill.org", "kathryn.powell@bryantpattengill.org" },
            //    Positions = new List<string>() { "PTO Council (\"PTOC\") Representative 2018/19" }
            //});
            //members.Add(new BoardMember()
            //{
            //    Id = "ruth-hamstra",
            //    Name = "Ruth Hamstra",
            //    IsCurrent = false,
            //    //Phone = new List<string>() { "" },
            //    //Email = new List<string>() { "" },
            //    Positions = new List<string>() { "Co-President 2013/14" }
            //});
            //members.Add(new BoardMember()
            //{
            //    Id = "elaine-cohen",
            //    Name = "Elaine Cohen",
            //    IsCurrent = false,
            //    //Phone = new List<string>() { "" },
            //    //Email = new List<string>() { "" },
            //    Positions = new List<string>() { "Co-Treasurer 2013/14" }
            //});
            //members.Add(new BoardMember()
            //{
            //    Id = "deanna-mercado",
            //    Name = "Deanna Mercado",
            //    IsCurrent = false,
            //    //Phone = new List<string>() { "" },
            //    //Email = new List<string>() { "" },
            //    Positions = new List<string>() { "External Secretary 2013/14" }
            //});
            //members.Add(new BoardMember()
            //{
            //    Id = "caroline-vitale",
            //    Name = "Caroline Vitale",
            //    IsCurrent = false,
            //    //Phone = new List<string>() { "" },
            //    //Email = new List<string>() { "" },
            //    Positions = new List<string>() { "PTOC Representative 2013/14" }
            //});
            //members.Add(new BoardMember()
            //{
            //    Id = "shelley-bruder",
            //    Name = "Shelley Bruder",
            //    IsCurrent = false,
            //    //Phone = new List<string>() { "" },
            //    //Email = new List<string>() { "" },
            //    Positions = new List<string>() { "Interim Bryant Co-Principal Fall 2017" }
            //});
            //members.Add(new BoardMember()
            //{
            //    Id = "kathy-morhous",
            //    Name = "Kathy Morhous",
            //    IsCurrent = false,
            //    //Phone = new List<string>() { "" },
            //    //Email = new List<string>() { "" },
            //    Positions = new List<string>() { "Interim Bryant Co-Principal Fall 2017" }
            //});

            //members.Add(new BoardMember()
            //{
            //    Id = "jamar-humphrey",
            //    Name = "Jamar Humphrey",
            //    IsCurrent = true,
            //    Phone = new List<string>() { "(734) 997-1212" },
            //    Email = new List<string>() { "principals@bryantpattengill.org", "humphrey@aaps.k12.mi.us" },
            //    Positions = new List<string>() { "Bryant Principal 2018/19", "Bryant Associate Principal Spring 2018" },
            //    Image = "/images/profile/jamar-humphrey.png"
            //});
            //members.Add(new BoardMember()
            //{
            //    Id = "susan-maurus",
            //    Name = "Susan Maurus",
            //    IsCurrent = true,
            //    Phone = new List<string>() { "734-994-1961" },
            //    Email = new List<string>() { "principals@bryantpattengill.org", "mauruss@aaps.k12.mi.us" },
            //    Positions = new List<string>() { "Pattengill Principal 2018/19", "Pattengill Associate Principal Spring 2018" }
            //});
            //BoardMembers = members;

            #endregion

        }

        public void LoadEvents(string jsonFile)
        {
            string rawString = File.ReadAllText(jsonFile);


            var jss = new JsonSerializerSettings
            {
                DateFormatHandling = DateFormatHandling.IsoDateFormat,
                DateTimeZoneHandling = DateTimeZoneHandling.Utc,
                DateParseHandling = DateParseHandling.DateTimeOffset,
                
            };
            Events = JsonConvert.DeserializeObject<List<Event>>(rawString, jss);
            //return responseObj.Select(s => new {
            //    id = s["id"].Value<int>(),
            //    date = s["date"].Value<DateTimeOffset>().DateTime,
            //});

            //Events = JsonConvert.DeserializeObject<List<Event>>(rawString);

            #region Old Code

            //var events = new List<Event>();

            //events.Add(new Event()
            //{
            //    Start = new DateTime(2017, 8, 29, 4 + 12, 0, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2017, 8, 29, 6 + 12, 0, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Popsicles on the Playground",
            //    Location = "Bryant Playground",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.Social
            //});

            //events.Add(new Event()
            //{
            //    Start = new DateTime(2017, 9, 5, 0, 0, 0, 0, DateTimeKind.Local),
            //    End = null,
            //    IsAllDay = true,
            //    Title = "First Day of 2017-18 School Year",
            //    ShortDescription = "Welcome! School Days: 8:56am – 3:59pm",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.Informational
            //});

            //events.Add(new Event()
            //{
            //    Start = new DateTime(2017, 9, 12, 6 + 12, 30, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2017, 9, 12, 8 + 12, 0, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Pattengill Curriculum Night",
            //    Location = "Pattengill",
            //    Type = EventType.Pattengill,
            //    Category = CategoryType.CurriculumNight
            //});

            //events.Add(new Event()
            //{
            //    Start = new DateTime(2017, 9, 19, 6 + 12, 30, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2017, 9, 19, 8 + 12, 0, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Bryant Curriculum Night",
            //    Location = "Bryant",
            //    Type = EventType.Bryant,
            //    Category = CategoryType.CurriculumNight
            //});

            //events.Add(new Event()
            //{
            //    Start = new DateTime(2017, 9, 27, 0, 0, 0, 0, DateTimeKind.Local),
            //    End = null,
            //    IsAllDay = true,
            //    Title = "Pattengill Picture Day",
            //    Location = "Pattengill",
            //    Type = EventType.Pattengill,
            //    Category = CategoryType.PictureDay
            //});

            //events.Add(new Event()
            //{
            //    Start = new DateTime(2017, 9, 27, 0, 30, 0, 0, DateTimeKind.Local),
            //    End = null,
            //    IsAllDay = true,
            //    Title = "Early Release Day",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.EarlyRelease
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2017, 10, 3, 4 + 12, 30, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2017, 10, 3, 5 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "School Improvement Team (SIT) Meeting",
            //    Location = "Bryant",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.SITMeeting
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2017, 10, 3, 6 + 12, 0, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2017, 10, 3, 8 + 12, 0, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "PTO Board (budget) Meeting",
            //    Location = "Bryant Media Center",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.PTOBoardMeeting,
            //    Url = "/board"
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2017, 10, 4, 0, 0, 0, 0, DateTimeKind.Local),
            //    End = null,
            //    IsAllDay = true,
            //    Title = "Bryant Picture Day",
            //    Type = EventType.Bryant,
            //    Category = CategoryType.PictureDay
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2017, 10, 4, 0, 0, 1, 0, DateTimeKind.Local),
            //    End = null,
            //    IsAllDay = true,
            //    Title = "Student Count Day",
            //    ShortDescription = "please be sure your student(s) attend this day",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.CountDay
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2017, 10, 4, 8, 30, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2017, 10, 4, 5 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Parent Lending Library",
            //    Location = "Bryant Lobby",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.ParentLendingLibrary,
            //    Url = "/events/parent-lending-library"
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2017, 10, 6, 4 + 12, 0, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2017, 10, 6, 5 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Academic Games",
            //    Location = "Pattengill Media Center",
            //    ShortDescription = "Learn different math and English skills through the joy of games. Parents can contact Praveen at (734) 355-4295 or PattengillAG@gmail.com.",
            //    Type = EventType.Pattengill,
            //    Category = CategoryType.AcademicGames,
            //    Url = "/events/academic-games"
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2017, 10, 11, 8, 30, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2017, 10, 11, 5 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Parent Lending Library",
            //    Location = "Bryant Lobby",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.ParentLendingLibrary,
            //    Url = "/events/parent-lending-library"
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2017, 10, 12, 6 + 12, 30, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2017, 10, 12, 7 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "PTO Fall General Meeting",
            //    Location = "Bryant",
            //    ShortDescription = "ALL Families Welcome, Childcare Provided",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.PTOGeneralMeeting,
            //    Url = "/board"
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2017, 10, 13, 0, 0, 0, 0, DateTimeKind.Local),
            //    IsAllDay = true,
            //    Title = "RSVP Family Writing Workshop",
            //    Location = "Bryant",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.Workshop,
            //    ShortDescription = "Families experience the joy of sharing and writing stories about favorite play places. Contact Mrs. Lim at lims@aaps.k12.mi.us",
            //    Url = "https://drive.google.com/file/d/0B8uyyn6m4sL0TlVBX2JyS3hXUjQ/view"
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2017, 10, 13, 4 + 12, 0, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2017, 10, 13, 5 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Academic Games",
            //    Location = "Pattengill Media Center",
            //    ShortDescription = "Learn different math and English skills through the joy of games. Parents can contact Praveen at (734) 355-4295 or PattengillAG@gmail.com.",
            //    Type = EventType.Pattengill,
            //    Category = CategoryType.AcademicGames,
            //    Url = "/events/academic-games"
            //});

            //events.Add(new Event()
            //{
            //    Start = new DateTime(2017, 10, 18, 0, 0, 0, DateTimeKind.Local),
            //    //End = new DateTime(2017, 10, 18, 0, 0, 0, 0, DateTimeKind.Local),
            //    IsAllDay = true,
            //    Title = "Spirit Wear Order Forms Due",
            //    //Location = "Bryant Lobby",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.Informational,
            //    //Url = "/events/parent-lending-library"
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2017, 10, 18, 8, 30, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2017, 10, 18, 5 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Parent Lending Library",
            //    Location = "Bryant Lobby",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.ParentLendingLibrary,
            //    Url = "/events/parent-lending-library"
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2017, 10, 19, 4 + 12, 15, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2017, 10, 19, 6 + 12, 15, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Family Writing Workshop",
            //    Location = "Bryant",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.Workshop,
            //    ShortDescription = "Families experience the joy of sharing and writing stories about favorite play places.",
            //    Url = "https://drive.google.com/file/d/0B8uyyn6m4sL0TlVBX2JyS3hXUjQ/view"
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2017, 10, 19, 5 + 12, 30, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2017, 10, 19, 6 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Girl Scouts Parent/Information Meeting",
            //    Location = "Pattengill Art Room",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.GirlScouts,
            //    ShortDescription = "This meeting is for parents and their daughters.",
            //    Url = "/events/girl-scouts"
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2017, 10, 20, 4 + 12, 0, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2017, 10, 20, 5 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Academic Games",
            //    Location = "Pattengill Media Center",
            //    ShortDescription = "Learn different math and English skills through the joy of games. Parents can contact Praveen at (734) 355-4295 or PattengillAG@gmail.com.",
            //    Type = EventType.Pattengill,
            //    Category = CategoryType.AcademicGames,
            //    Url = "/events/academic-games"
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2017, 10, 20, 5 + 12, 30, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2017, 10, 20, 8 + 12, 0, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Family Movie Night",
            //    Location = "Bryant Gym",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.Social,
            //    Url = "/events/movie-night"
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2017, 10, 24, 12 + 3, 0, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2017, 10, 24, 12 + 5, 0, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Shopping Signup Assistance",
            //    Location = "Bryant Foyer",
            //    ShortDescription = "Your family can raise money for our schools at $0 cost to you simply by shopping at Busch's, Kroger, Office Max/Office Depot or Amazon.com",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.Informational,
            //    Url = "https://drive.google.com/file/d/0B8uyyn6m4sL0OVRneU1zNkRnY0FibVpSS2txT19UTE9mR21F/view?usp=sharing"
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2017, 10, 25, 0, 0, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2017, 10, 25, 0, 0, 0, 0, DateTimeKind.Local),
            //    IsAllDay = true,
            //    Title = "Early Release Day",
            //    ShortDescription = "Students Dismissed at 1:29pm",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.EarlyRelease
            //});

            //events.Add(new Event()
            //{
            //    Start = new DateTime(2017, 10, 25, 11, 0, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2017, 10, 25, 12 + 9, 0, 0, 0, DateTimeKind.Local),
            //    IsAllDay = true,
            //    Title = "Restaurant Day",
            //    Location = "California Pizza Kitchen",
            //    ShortDescription = "Bring flyer and PTO will get 20% of the total bill. Good 10/23-10/29",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.RestaurantDay,
            //    Url = "https://drive.google.com/file/d/0B8uyyn6m4sL0UHRGeFh6bEF1NUU/view"
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2017, 10, 26, 4 + 12, 15, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2017, 10, 26, 6 + 12, 15, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Family Writing Workshop",
            //    Location = "Bryant",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.Workshop,
            //    ShortDescription = "Families experience the joy of sharing and writing stories about favorite play places.",
            //    Url = "https://drive.google.com/file/d/0B8uyyn6m4sL0TlVBX2JyS3hXUjQ/view"
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2017, 10, 27, 5 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = true,
            //    Title = "Box Tops for Education (Last Day)",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.BoxTops,
            //    ShortDescription = "The first of our two Box Tops collection days. There is a collection box at each school. Questions? Contact Jean Powell jmpowl@aol.com.",
            //    Url = "/fundraising/box-tops-for-education"
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2017, 10, 27, 4 + 12, 0, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2017, 10, 27, 5 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Academic Games",
            //    Location = "Pattengill Media Center",
            //    ShortDescription = "Learn different math and English skills through the joy of games. Parents can contact Praveen at (734) 355-4295 or PattengillAG@gmail.com.",
            //    Type = EventType.Pattengill,
            //    Category = CategoryType.AcademicGames,
            //    Url = "/events/academic-games"
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2017, 10, 31, 10, 45, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2017, 10, 31, 11, 15, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Bryant School Costume Parade",
            //    Location = "Bryant",
            //    Type = EventType.Bryant,
            //    Category = CategoryType.Informational,
            //    ShortDescription = "Bryant will have an inside the school parade at 10:45 a.m. on Halloween.",
            //    Url = "https://drive.google.com/file/d/0B8uyyn6m4sL0c0xfZjdWbk1PT1U/view?usp=sharing"
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2017, 11, 1, 8, 30, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2017, 11, 1, 5 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Parent Lending Library",
            //    Location = "Bryant Lobby",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.ParentLendingLibrary,
            //    Url = "/events/parent-lending-library"
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2017, 11, 2, 4 + 12, 15, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2017, 11, 2, 6 + 12, 15, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Family Writing Workshop",
            //    Location = "Bryant",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.Workshop,
            //    ShortDescription = "Families experience the joy of sharing and writing stories about favorite play places.",
            //    Url = "https://drive.google.com/file/d/0B8uyyn6m4sL0TlVBX2JyS3hXUjQ/view"
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2017, 11, 3, 4 + 12, 0, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2017, 11, 3, 5 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Academic Games",
            //    Location = "Pattengill Media Center",
            //    ShortDescription = "Learn different math and English skills through the joy of games. Parents can contact Praveen at (734) 355-4295 or PattengillAG@gmail.com.",
            //    Type = EventType.Pattengill,
            //    Category = CategoryType.AcademicGames,
            //    Url = "/events/academic-games"
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2017, 11, 7, 0, 0, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2017, 11, 7, 0, 0, 0, 0, DateTimeKind.Local),
            //    IsAllDay = true,
            //    Title = "Election Day",
            //    ShortDescription = "No School for Students",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.NoSchool
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2017, 11, 7, 0, 00, 0, 0, DateTimeKind.Local),
            //    End = null,
            //    IsAllDay = true,
            //    Title = "Revel & Roll FUNdraiser",
            //    Location = "Revel & Roll, 1950 S. Industrial Hwy, Ann Arbor, MI 48104",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.Fundraiser,
            //    ShortDescription = "See flyer. Cards sold 11-1pm or 4-6pm.",
            //    Url = "https://drive.google.com/file/d/0B8uyyn6m4sL0N0FpU1cxWWptNV81Z3ZCaDE4c2tRN1dZTnhF/view?usp=sharing"
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2017, 11, 8, 8, 30, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2017, 11, 8, 5 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Parent Lending Library",
            //    Location = "Bryant Lobby",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.ParentLendingLibrary,
            //    Url = "/events/parent-lending-library"
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2017, 11, 8, 0, 0, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2017, 11, 10, 0, 0, 0, 0, DateTimeKind.Local),
            //    IsAllDay = true,
            //    Title = "5th Grade Camp",
            //    Type = EventType.Pattengill,
            //    Url = "/events/5th-grade-camp/",
            //    Category = CategoryType.FieldTrip
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2017, 11, 10, 4 + 12, 0, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2017, 11, 10, 5 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Academic Games",
            //    Location = "Pattengill Media Center",
            //    ShortDescription = "Learn different math and English skills through the joy of games. Parents can contact Praveen at (734) 355-4295 or PattengillAG@gmail.com.",
            //    Type = EventType.Pattengill,
            //    Category = CategoryType.AcademicGames,
            //    Url = "/events/academic-games"
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2017, 11, 13, 6 + 12, 0, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2017, 11, 13, 7 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Fall Community Meeting",
            //    Location = "BRYANT School, Multipurpose Room",
            //    ShortDescription = "Share leadership updates and gather your input about the next chapter for Bryant and the Super Pair",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.CommunityMeeting,
            //    Url = "https://drive.google.com/open?id=1mmvBPGyZoRgEa2Vf7qSl-OMr87qgHBdt"
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2017, 11, 14, 0, 0, 0, 0, DateTimeKind.Local),
            //    End = null,
            //    IsAllDay = true,
            //    Title = "Pattengill Picture Retakes",
            //    Type = EventType.Pattengill,
            //    Category = CategoryType.PictureDay
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2017, 11, 14, 6 + 12, 0, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2017, 11, 14, 7 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "PTO Board Meeting",
            //    Location = "Pattengill Media Center",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.PTOBoardMeeting,
            //    Url = "/board"
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2017, 11, 15, 8, 30, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2017, 11, 15, 5 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Parent Lending Library",
            //    Location = "Bryant Lobby",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.ParentLendingLibrary,
            //    Url = "/events/parent-lending-library"
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2017, 11, 16, 1, 0, 0, 0, DateTimeKind.Local),
            //    End = null,
            //    IsAllDay = true,
            //    Title = "Bryant Picture Retakes",
            //    Type = EventType.Bryant,
            //    Category = CategoryType.PictureDay
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2017, 11, 16, 4 + 12, 15, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2017, 11, 16, 6 + 12, 15, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Family Writing Workshop",
            //    Location = "Bryant",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.Workshop,
            //    ShortDescription = "Families experience the joy of sharing and writing stories about favorite play places.",
            //    Url = "https://drive.google.com/file/d/0B8uyyn6m4sL0TlVBX2JyS3hXUjQ/view"
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2017, 11, 17, 4 + 12, 0, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2017, 11, 17, 5 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Academic Games",
            //    Location = "Pattengill Media Center",
            //    ShortDescription = "Learn different math and English skills through the joy of games. Parents can contact Praveen at (734) 355-4295 or PattengillAG@gmail.com.",
            //    Type = EventType.Pattengill,
            //    Category = CategoryType.AcademicGames,
            //    Url = "/events/academic-games"
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2017, 11, 22, 0, 0, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2017, 11, 24, 0, 0, 0, 0, DateTimeKind.Local),
            //    IsAllDay = true,
            //    Title = "Thanksgiving Break",
            //    ShortDescription = "No School for Students",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.PictureDay
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2017, 11, 27, 6, 0, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2017, 11, 27, 7, 0, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Community Meeting",
            //    ShortDescription = "Gather with AAPS leadership to discuss the results of the new Bryant and Pattengill principal surveys, as we prepare to find leaders best fit for our paw-some school community.",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.CommunityMeeting,
            //    Location = "Bryant Multipurpose Room"
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2017, 11, 29, 8, 30, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2017, 11, 29, 5 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Parent Lending Library",
            //    Location = "Bryant Lobby",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.ParentLendingLibrary,
            //    Url = "/events/parent-lending-library"
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2017, 12, 1, 4 + 12, 0, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2017, 12, 1, 5 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Academic Games",
            //    Location = "Pattengill Media Center",
            //    ShortDescription = "Learn different math and English skills through the joy of games. Parents can contact Praveen at (734) 355-4295 or PattengillAG@gmail.com.",
            //    Type = EventType.Pattengill,
            //    Category = CategoryType.AcademicGames
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2017, 12, 4, 0, 0, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2017, 12, 8, 0, 0, 0, 0, DateTimeKind.Local),
            //    IsAllDay = true,
            //    Title = "Pattengill Book Fair",
            //    Location = "Pattengill",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.Fundraiser
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2017, 12, 6, 8, 30, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2017, 12, 6, 5 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Parent Lending Library",
            //    Location = "Bryant Lobby",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.ParentLendingLibrary,
            //    Url = "/events/parent-lending-library"
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2017, 12, 7, 6 + 12, 0, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2017, 12, 7, 7 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Pattengill Book Fair Family Night",
            //    Location = "Pattengill",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.Social
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2017, 12, 7, 6 + 12, 0, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2017, 12, 7, 7 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Tech Night",
            //    Location = "Pattengill",
            //    Type = EventType.BryantAndPattengill,
            //    Url = "/events/tech-night/",
            //    Category = CategoryType.TechNight
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2017, 12, 8, 4 + 12, 0, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2017, 12, 8, 5 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Academic Games",
            //    Location = "Pattengill Media Center",
            //    ShortDescription = "Learn different math and English skills through the joy of games. Parents can contact Praveen at (734) 355-4295 or PattengillAG@gmail.com.",
            //    Type = EventType.Pattengill,
            //    Category = CategoryType.AcademicGames,
            //    Url = "/events/academic-games"
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2017, 12, 13, 8, 30, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2017, 12, 13, 5 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Parent Lending Library",
            //    Location = "Bryant Lobby",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.ParentLendingLibrary,
            //    Url = "/events/parent-lending-library"
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2017, 12, 15, 4 + 12, 0, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2017, 12, 15, 5 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Academic Games",
            //    Location = "Pattengill Media Center",
            //    ShortDescription = "Learn different math and English skills through the joy of games. Parents can contact Praveen at (734) 355-4295 or PattengillAG@gmail.com.",
            //    Type = EventType.Pattengill,
            //    Category = CategoryType.AcademicGames,
            //    Url = "/events/academic-games"
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2017, 12, 20, 8, 30, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2017, 12, 20, 5 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Parent Lending Library",
            //    Location = "Bryant Lobby",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.ParentLendingLibrary,
            //    Url = "/events/parent-lending-library"
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2017, 12, 25, 0, 0, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2018, 1, 5, 0, 0, 0, 0, DateTimeKind.Local),
            //    IsAllDay = true,
            //    Title = "Winter Break",
            //    ShortDescription = "No School for Students",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.NoSchool
            //});

            //// JAN
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 1, 8, 0, 0, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2018, 1, 8, 0, 0, 0, 0, DateTimeKind.Local),
            //    IsAllDay = true,
            //    Title = "School Resumes",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.Informational
            //});

            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 1, 10, 8, 30, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2018, 1, 10, 5 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Parent Lending Library",
            //    Location = "Bryant Lobby",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.ParentLendingLibrary,
            //    Url = "/events/parent-lending-library"
            //});

            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 1, 11, 10, 15, 0, 0, DateTimeKind.Local),
            //    End = null,
            //    IsAllDay = false,
            //    Title = "Bryant MLK Day Assembly",
            //    Location = "Bryant Gym",
            //    Type = EventType.Bryant,
            //    Category = CategoryType.Assembly
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 1, 11, 0, 0, 0, 0, DateTimeKind.Local),
            //    End = null,
            //    IsAllDay = true,
            //    Title = "Bryant Yearbook Group Pictures",
            //    Type = EventType.Bryant,
            //    Category = CategoryType.PictureDay
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 1, 15, 0, 0, 0, 0, DateTimeKind.Local),
            //    End = null,
            //    IsAllDay = true,
            //    Title = "Martin Luther King Jr. Day",
            //    ShortDescription = "No School for Students",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.NoSchool
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 1, 16, 5 + 12, 30, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2018, 1, 16, 6 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Meet and Greet Event #1",
            //    ShortDescription = "Please come and welcome our new associate principals, Jamar Humphrey (Bryant) and Susan Maurus (Pattengill)",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.CommunityMeeting
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 1, 17, 0, 0, 0, 0, DateTimeKind.Local),
            //    End = null,
            //    IsAllDay = true,
            //    Title = "Pattengill Talent Show Tryouts",
            //    Type = EventType.Pattengill,
            //    Category = CategoryType.TalentShow
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 1, 17, 8, 30, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2018, 1, 17, 5 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Parent Lending Library",
            //    Location = "Bryant Lobby",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.ParentLendingLibrary,
            //    Url = "/events/parent-lending-library"
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 1, 18, 0, 0, 0, 0, DateTimeKind.Local),
            //    End = null,
            //    IsAllDay = true,
            //    Title = "Pattengill Yearbook Group Pictures",
            //    Type = EventType.Pattengill,
            //    Category = CategoryType.PictureDay
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 1, 18, 4 + 12, 0, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2018, 1, 18, 5 + 12, 0, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Meet and Greet Event #2",
            //    ShortDescription = "Please come and welcome our new associate principals, Jamar Humphrey (Bryant) and Susan Maurus (Pattengill)",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.CommunityMeeting
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 1, 23, 6 + 12, 0, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2018, 1, 23, 7 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Family Science Night",
            //    Location = "Bryant Gym",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.ScienceNight
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 1, 24, 8, 30, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2018, 1, 24, 5 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Parent Lending Library",
            //    Location = "Bryant Lobby",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.ParentLendingLibrary,
            //    Url = "/events/parent-lending-library"
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 1, 31, 0, 0, 0, 0, DateTimeKind.Local),
            //    End = null,
            //    IsAllDay = true,
            //    Title = "Early Release Day",
            //    ShortDescription = "Students Dismissed at 1:29pm",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.EarlyRelease
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 1, 31, 0, 0, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2018, 1, 31, 0, 0, 0, 0, DateTimeKind.Local),
            //    IsAllDay = true,
            //    Title = "Restaurant Night at Potbelly",
            //    ShortDescription = "Potbelly Sandwich Shop will donate 25% of the total bill to the Bryant-Pattengill PTO.",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.RestaurantDay,
            //    Location = "Potbelly Sandwich Shop, 980 W Eisenhower Pkwy, Unit C."
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 1, 31, 8, 30, 0, 0, DateTimeKind.Local),
            //    End = null, // unsure what time will go to
            //    IsAllDay = false,
            //    Title = "Parent Lending Library",
            //    Location = "Bryant Lobby",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.ParentLendingLibrary,
            //    Url = "/events/parent-lending-library"
            //});
            //// feb
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 2, 1, 6 + 12, 0, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2018, 2, 1, 7 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "PTO Board Meeting",
            //    Location = "Bryant Media Center",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.PTOBoardMeeting,
            //    Url = "/board"

            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 2, 7, 9, 45, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2018, 2, 7, 11, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "2nd Grade to Pattengill To See Talent Show",
            //    Type = EventType.Bryant,
            //    Category = CategoryType.TalentShow
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 2, 7, 8, 30, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2018, 2, 7, 5 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Parent Lending Library",
            //    Location = "Bryant Lobby",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.ParentLendingLibrary,
            //    Url = "/events/parent-lending-library"
            //});

            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 2, 12, 0, 0, 0, 0, DateTimeKind.Local),
            //    End = null,
            //    IsAllDay = true,
            //    Title = "NAAPID/Parent Involvement Day",
            //    ShortDescription = "Date changed to Feb. 12th. ALL Parents Welcome; Morning: Bryant, Afternoon: Pattengill",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.Naapid
            //});

            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 2, 14, 0, 0, 0, 0, DateTimeKind.Local),
            //    End = null,
            //    IsAllDay = true,
            //    Title = "Student Count Day",
            //    ShortDescription = "Please be sure your student(s) attend this day",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.CountDay
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 2, 14, 8, 30, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2018, 2, 14, 5 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Parent Lending Library",
            //    Location = "Bryant Lobby",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.ParentLendingLibrary,
            //    Url = "/events/parent-lending-library"
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 2, 16, 0, 0, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2018, 2, 19, 0, 0, 0, 0, DateTimeKind.Local),
            //    IsAllDay = true,
            //    Title = "Mid-Winter Break",
            //    ShortDescription = "No School for Students",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.NoSchool
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 2, 21, 6 + 12, 0, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2018, 2, 21, 7 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Pattengill Science Fair",
            //    Location = "Pattengill Gym",
            //    Type = EventType.Pattengill,
            //    Category = CategoryType.ScienceFair
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 2, 21, 8, 30, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2018, 2, 21, 5 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Parent Lending Library",
            //    Location = "Bryant Lobby",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.ParentLendingLibrary,
            //    Url = "/events/parent-lending-library"
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 2, 22, 6 + 12, 0, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2018, 2, 22, 7 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = true,
            //    Title = "Kindergarten/Young Fives Round-Up",
            //    Location = "Bryant Multipurpose Room",
            //    Type = EventType.Bryant,
            //    Category = CategoryType.KYoung5RoundUp
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 2, 23, 7 + 12, 0, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2018, 2, 23, 9 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Rescheduled Pattengill Talent Show for Parents/Community",
            //    Location = "Pattengill",
            //    ShortDescription = "Date updated to the Feb. 23rd",
            //    Type = EventType.Pattengill,
            //    Category = CategoryType.TalentShow
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 2, 28, 0, 0, 0, 0, DateTimeKind.Local),
            //    End = null,
            //    IsAllDay = true,
            //    Title = "Early Release Day",
            //    ShortDescription = "Students Dismissed at 1:29pm",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.EarlyRelease
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 2, 28, 8, 30, 0, 0, DateTimeKind.Local),
            //    End = null,
            //    IsAllDay = false,
            //    Title = "Parent Lending Library",
            //    Location = "Bryant Lobby",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.ParentLendingLibrary,
            //    Url = "/events/parent-lending-library"
            //});
            //// march
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 3, 2, 0, 0, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2018, 3, 14, 0, 0, 0, 0, DateTimeKind.Local),
            //    IsAllDay = true,
            //    Title = "Read-a-Thon",
            //    Type = EventType.BryantAndPattengill,
            //    Url = "/events/read-a-thon/",
            //    Category = CategoryType.ReadAThon
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 3, 5, 0, 0, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2018, 3, 9, 0, 0, 0, 0, DateTimeKind.Local),
            //    IsAllDay = true,
            //    Title = "Bryant Book Fair",
            //    Location = "Bryant",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.BookFair
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 3, 6, 4 + 12, 30, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2018, 3, 6, 5 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "School Improvement Team (SIT) Meeting",
            //    Location = "Pattengill",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.SITMeeting
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 3, 6, 6 + 12, 0, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2018, 3, 6, 7 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "PTO Board Meeting",
            //    Location = "Pattengill Media Center",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.PTOBoardMeeting,
            //    Url = "/board"
            //});

            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 3, 7, 8, 30, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2018, 3, 7, 5 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Parent Lending Library",
            //    Location = "Bryant Lobby",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.ParentLendingLibrary,
            //    Url = "/events/parent-lending-library"
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 3, 8, 6 + 12, 30, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2018, 3, 8, 8 + 12, 0, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Pajama Night & Bryant Book Fair Family Night",
            //    Location = "Bryant Media Center",
            //    Type = EventType.Bryant,
            //    Category = CategoryType.BookFair
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 3, 14, 8, 30, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2018, 3, 14, 5 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Parent Lending Library",
            //    Location = "Bryant Lobby",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.ParentLendingLibrary,
            //    Url = "/events/parent-lending-library"
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 3, 15, 0, 0, 0, 0, DateTimeKind.Local),
            //    End = null,
            //    IsAllDay = true,
            //    Title = "Disability Awareness Workshop (4th grade)",
            //    Location = "Pattengill",
            //    ShortDescription = "Morning",
            //    Type = EventType.Pattengill,
            //    Category = CategoryType.Assembly
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 3, 20, 7 + 12, 0, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2018, 3, 20, 8 + 12, 0, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "4th Grade Spring Recorder Concert",
            //    Location = "Pattengill Cafeteria",
            //    Type = EventType.Pattengill,
            //    Category = CategoryType.Concert
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 3, 21, 8, 30, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2018, 3, 21, 5 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Parent Lending Library",
            //    Location = "Bryant Lobby",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.ParentLendingLibrary,
            //    Url = "/events/parent-lending-library"
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 3, 26, 0, 0, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2018, 3, 30, 0, 0, 0, 0, DateTimeKind.Local),
            //    IsAllDay = true,
            //    Title = "Spring Break",
            //    Location = "No School for Students",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.NoSchool
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 3, 28, 8, 30, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2018, 3, 28, 5 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Parent Lending Library",
            //    Location = "Bryant Lobby",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.ParentLendingLibrary,
            //    Url = "/events/parent-lending-library"
            //});
            //// apr
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 4, 2, 0, 0, 0, 0, DateTimeKind.Local),
            //    End = null,
            //    IsAllDay = true,
            //    Title = "School Resumes",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.Informational
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 4, 4, 8, 30, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2018, 4, 4, 5 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Parent Lending Library",
            //    Location = "Bryant Lobby",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.ParentLendingLibrary,
            //    Url = "/events/parent-lending-library"
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 4, 10, 9, 0, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2018, 4, 10, 2 + 12, 0, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Kindergarten Registration and Readiness",
            //    Location = "Bryant Gym",
            //    ShortDescription = "(by appointment)",
            //    Type = EventType.Bryant,
            //    Category = CategoryType.Registration
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 4, 10, 6 + 12, 0, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2018, 4, 10, 7 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "PTO Board Meeting",
            //    Location = "Bryant Media Center",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.PTOBoardMeeting,
            //    Url = "/board"
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 4, 11, 8, 30, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2018, 4, 11, 5 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Parent Lending Library",
            //    Location = "Bryant Lobby",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.ParentLendingLibrary,
            //    Url = "/events/parent-lending-library"
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 4, 12, 0, 0, 0, 0, DateTimeKind.Local),
            //    End = null,
            //    IsAllDay = true,
            //    Title = "Bryant Spring Picture Day",
            //    Type = EventType.Bryant,
            //    Category = CategoryType.PictureDay
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 4, 17, 6 + 12, 0, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2018, 4, 17, 7 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Volunteer Appreciation Night",
            //    Location = "Bryant",
            //    ShortDescription = "ALL Family Volunteers Welcome",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.VolunteerAppreciation
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 4, 18, 0, 0, 0, 0, DateTimeKind.Local),
            //    End = null,
            //    IsAllDay = true,
            //    Title = "Pattengill Spring Picture Day",
            //    Type = EventType.Pattengill,
            //    Category = CategoryType.PictureDay
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 4, 18, 8, 30, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2018, 4, 18, 5 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Parent Lending Library",
            //    Location = "Bryant Lobby",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.ParentLendingLibrary,
            //    Url = "/events/parent-lending-library"
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 4, 20, 0, 0, 0, 0, DateTimeKind.Local),
            //    End = null,
            //    IsAllDay = true,
            //    Title = "Earth Day of Service",
            //    ShortDescription = "ALL Parents Welcome To Volunteer; Morning: Bryant, Afternoon: Pattengill",
            //    Type = EventType.Pattengill,
            //    Category = CategoryType.ServiceDay
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 4, 24, 7 + 12, 0, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2018, 4, 24, 8 + 12, 0, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "3rd Grade Spring Concert",
            //    Location = "Pattengill Cafeteria",
            //    Type = EventType.Pattengill,
            //    Category = CategoryType.Concert
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 4, 25, 8, 30, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2018, 4, 25, 5 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Parent Lending Library",
            //    Location = "Bryant Lobby",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.ParentLendingLibrary,
            //    Url = "/events/parent-lending-library"
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 4, 26, 6 + 12, 30, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2018, 4, 26, 8 + 12, 0, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "International Festival",
            //    Location = "Pattengill",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.InternationalFestival,
            //    Url = "/events/international-festival"
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 4, 29, 11, 0, 0, 0, DateTimeKind.Local),
            //    End = null,
            //    IsAllDay = true,
            //    Title = "Restaurant Day",
            //    Location = "Buffalo Wild Wings @ 3150 Boardwalk Dr",
            //    ShortDescription = "Bring flyer and PTO will get 20% of purchases. (734) 997-9464",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.RestaurantDay,
            //    Url = "https://drive.google.com/open?id=1YxZsZDRgKFic4VNAHMF_1NQUqRk1WX95"
            //});
            //// may
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 5, 2, 9, 30, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2018, 5, 2, 11, 0, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "2nd to 3rd Grade Transition (Students)",
            //    Location = "Pattengill",
            //    ShortDescription = "Students at Pattengill",
            //    Type = EventType.Bryant,
            //    Category = CategoryType.FieldTrip
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 5, 2, 6 + 12, 0, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2018, 5, 2, 7 + 12, 0, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "2nd to 3rd Grade Transition (Parents)",
            //    Location = "Pattengill",
            //    ShortDescription = "Parent Informational Night",
            //    Type = EventType.Bryant,
            //    Category = CategoryType.TransitionMeeting
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 5, 2, 8, 30, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2018, 5, 2, 5 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Parent Lending Library",
            //    Location = "Bryant Lobby",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.ParentLendingLibrary,
            //    Url = "/events/parent-lending-library"
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 5, 7, 0, 0, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2018, 5, 11, 0, 0, 0, 0, DateTimeKind.Local),
            //    IsAllDay = true,
            //    Title = "Teacher & Staff Appreciation Week",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.Informational
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 5, 7, 6, 30, 0, 0, DateTimeKind.Local),
            //    End = null,
            //    IsAllDay = false,
            //    Title = "5th Grade Instrumental Spring Concert",
            //    Location = "Pattengill Cafeteria",
            //    Type = EventType.Pattengill,
            //    Category = CategoryType.Concert
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 5, 8, 0, 0, 0, 0, DateTimeKind.Local),
            //    End = null,
            //    IsAllDay = true,
            //    Title = "Election Day",
            //    ShortDescription = "No School for Students",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.NoSchool
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 5, 9, 8, 30, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2018, 5, 9, 5 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Parent Lending Library",
            //    Location = "Bryant Lobby",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.ParentLendingLibrary,
            //    Url = "/events/parent-lending-library"
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 5, 10, 4 + 12, 30, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2018, 5, 10, 5 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "School Improvement Team (SIT) Meeting",
            //    Location = "Pattengill",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.SITMeeting
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 5, 10, 6 + 12, 0, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2018, 5, 10, 7 + 12, 0, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "PTO Spring General Meeting",
            //    Location = "Pattengill",
            //    ShortDescription = "ALL Families Welcome, Childcare Provided",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.PTOGeneralMeeting,
            //    Url = "/board"
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 5, 12, 0, 0, 0, 0, DateTimeKind.Local),
            //    End = null,
            //    IsAllDay = true,
            //    Title = "Washtenaw Elementary Science Olympiad",
            //    Location = "Pioneer High School",
            //    Type = EventType.BryantAndPattengill,//previously /so/
            //    Url = "/events/science-olympiad",
            //    Category = CategoryType.ScienceOlympiad
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 5, 16, 8, 30, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2018, 5, 16, 5 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Parent Lending Library",
            //    Location = "Bryant Lobby",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.ParentLendingLibrary,
            //    Url = "/events/parent-lending-library"
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 5, 18, 6 + 12, 0, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2018, 5, 18, 8 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Bryant/Pattengill Ice Cream Social",
            //    Location = "Pattengill",
            //    Type = EventType.BryantAndPattengill,
            //    Url = "/events/ice-cream-social/",
            //    Category = CategoryType.Fundraiser
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 5, 23, 0, 0, 0, 0, DateTimeKind.Local),
            //    End = null,
            //    IsAllDay = true,
            //    Title = "Early Release Day",
            //    ShortDescription = "Students Dismissed at 1:29pm",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.EarlyRelease
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 5, 23, 8, 30, 0, 0, DateTimeKind.Local),
            //    End = null,
            //    IsAllDay = false,
            //    Title = "Parent Lending Library",
            //    Location = "Bryant Lobby",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.ParentLendingLibrary,
            //    Url = "/events/parent-lending-library"
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 5, 24, 6 + 12, 30, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2018, 5, 24, 7 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "2nd Grade Spring Sing",
            //    Location = "Bryant Gym",
            //    Type = EventType.Bryant,
            //    Category = CategoryType.Concert
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 5, 28, 0, 0, 0, 0, DateTimeKind.Local),
            //    End = null,
            //    IsAllDay = true,
            //    Title = "Memorial Day",
            //    ShortDescription = "No School for Students",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.NoSchool
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 5, 30, 8, 30, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2018, 5, 30, 5 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Parent Lending Library",
            //    Location = "Bryant Lobby",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.ParentLendingLibrary,
            //    Url = "/events/parent-lending-library"
            //});
            //// jun
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 6, 1, 1 + 12, 45, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2018, 6, 1, 3 + 12, 15, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Bryant Field Day",
            //    ShortDescription = "(Raindate: Monday, June 4)",
            //    Type = EventType.Bryant,
            //    Category = CategoryType.FieldDay
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 6, 5, 6 + 12, 0, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2018, 6, 5, 7 + 12, 30, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "PTO Board Meeting",
            //    Location = "Bryant Media Center",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.PTOBoardMeeting,
            //    Url = "/board"
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 6, 8, 1 + 12, 15, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2018, 6, 8, 3 + 12, 15, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Pattengill Field Day",
            //    ShortDescription = "(Raindate: Monday, June 11)",
            //    Type = EventType.Pattengill,
            //    Category = CategoryType.FieldDay
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 6, 14, 9, 30, 0, 0, DateTimeKind.Local),
            //    End = null,
            //    IsAllDay = false,
            //    Title = "2nd Grade Recognition Ceremony & \"Clap Out\"",
            //    Location = "Bryant Gym",
            //    ShortDescription = "Families Welcome",
            //    Type = EventType.Bryant,
            //    Category = CategoryType.Ceremony
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 6, 14, 2 + 12, 0, 0, 0, DateTimeKind.Local),
            //    End = null,
            //    IsAllDay = false,
            //    Title = "5th Grade Promotion Ceremony",
            //    Location = "Pattengill Cafeteria",
            //    ShortDescription = "Families Welcome",
            //    Type = EventType.Pattengill,
            //    Category = CategoryType.Ceremony
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 6, 15, 9, 0, 0, 0, DateTimeKind.Local),
            //    End = null,
            //    IsAllDay = false,
            //    Title = "Flag Ceremony & \"Clap Out\"",
            //    Location = "Pattengill Cafeteria",
            //    ShortDescription = "Families Welcome",
            //    Type = EventType.Pattengill,
            //    Category = CategoryType.Ceremony
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 6, 15, 0, 0, 0, 0, DateTimeKind.Local),
            //    End = null,
            //    IsAllDay = true,
            //    Title = "Last Day of School",
            //    ShortDescription = "Early Release at 11:39am; Report Cards Go Home",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.EarlyRelease
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 8, 28, 4 + 12, 0, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2018, 8, 28, 6 + 12, 0, 0, 0, DateTimeKind.Local),
            //    IsAllDay = false,
            //    Title = "Back-To-School Popsicles on the Playground",
            //    Location = "Bryant Playground",
            //    ShortDescription = "(TENTATIVE) Bryant/Pattengill families, please mark your calendars for our back-to-school event. Come out to see classmates old and new, visit with the principals, and share a frozen treat! Several members of the Bryant/Pattengill teaching staff will be available to meet and greet families. We look forward to seeing you in August, to kick off another great school year!",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.Social
            //});

            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 9, 4, 0, 0, 0, 0, DateTimeKind.Local),
            //    End = null,
            //    IsAllDay = true,
            //    Title = "First Day of 2018-19 School Year",
            //    ShortDescription = "Welcome!",
            //    Type = EventType.BryantAndPattengill,
            //    Category = CategoryType.Informational
            //});
            //events.Add(new Event()
            //{
            //    Start = new DateTime(2018, 11, 7, 0, 0, 0, 0, DateTimeKind.Local),
            //    End = new DateTime(2018, 11, 9, 0, 0, 0, 0, DateTimeKind.Local),
            //    IsAllDay = true,
            //    Title = "5th Grade Camp",
            //    Type = EventType.Pattengill,
            //    Location = "Howell Nature Center",
            //    ShortDescription = "Next year your 5th grader will have the opportunity to attend 5th Grade Camp. We encourage all 5th graders to experience this amazing overnight camp opportunity and make lifelong memories! Information packet to follow in the fall.",
            //    Url = "http://howellnaturecenter.org/school-programs/overnight-school-camp/",
            //    //Url = "/events/5th-grade-camp/",
            //    Category = CategoryType.FieldTrip
            //});
            //Events = events;
            #endregion
        }
    }
}
